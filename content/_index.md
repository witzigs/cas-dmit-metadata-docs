---
title: "Bibliothekarische Metadatenformate"
---
# Bibliothekarische Metadatenformate
Kurs im [CAS Datenmanagement und Informationstechnologien](https://www.biw.uzh.ch/de/casdmit.html), 5. und 12. April 2024 von Silvia Witzig

[Präsentation](https://witzigs.gitlab.io/cas-dmit-metadata-slides/)

### Lernziele  und Inhalte

Inhalte:
* Einführung zu Definition und Art von Metadaten
* Vorstellen verschiedener Formate, die man im Bibliotheksbereich antrifft (MARC, METS, MODS, DublinCore, Bibframe/RDF und weitere)
* Praxisorientierte Blöcke zum Beziehen und Publizieren von Daten (OAI-PMH, SRU, systemspezifische Schnittstellen, aber auch CSV-Dateien) und zum Analysieren und Transformieren von Daten (Mappings, mögliche Tools und Vorgehensweisen anhand von Beispielen)
 
Lernziele:
* Die Studierenden lernen Definition und Kategorien von Metadaten kennen und können Daten den jeweiligen Kategorien zuordnen.
* Sie kennen die bibliotheksspezifischen Standards und ihre Anwendungsbereiche.
* Sie kennen Vor- und Nachteile unterschiedlicher Metadatenstandards.
* Sie kennen im Bibliotheksbereich verbreitete Schnittstellen.
* Sie machen erste Erfahrungen in der praktischen Arbeit mit Metadaten.