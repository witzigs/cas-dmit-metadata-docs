---
title: "Neue interessante Dinge"
date: 2021-07-20T12:28:11+02:00
draft: true
---

* DNBLab mit Jupyter Notebooks für SRU und OAI: https://www.dnb.de/DE/Professionell/Services/WissenschaftundForschung/DNBLab/dnblab_node.html#doc731014bodyText4

* SRU: nur noch SRU 1.2 abdecken in Übung, 2.0 unterscheidet sich doch recht stark (in CH durch Alma ist 1.2 wohl am verbreitetsten)

* SRU: Reference for explain (zeerex) http://zeerex.z3950.org/dtd/commentary.html und DTD http://zeerex.z3950.org/dtd/index.html

* SRU: recordPacking, nicht recordPackaging!!!

* http://www.digitalhumanities.org/dhq/vol/3/3/000064/000064.html

* https://mobile.twitter.com/ExcelPope/status/1449363985464184836
Person 1: The glass is 1/2 full

Person 2: The glass is 1/2 empty

Excel: The glass is the 1st of February

https://data.slub-dresden.de/

* RERO+ und BIBFRAME

---

2025:

* Angebote zum Datenexport von Bibliotheken (ZB Jupyter Notebooks, swisscollections)
* https://blog.metaphacts.com/bibframe-dilemmas-for-libraries-challenges-and-opportunities