Liebe Silvia,

Danke für deine Nachricht. Dies ist schon ein spezifisches Thema, das nicht viele Leute interessiert :)

Daten-Struktur
Wir verwenden das Bibframe-Model vor allem für die Strukturierung der JSON Daten, und noch nicht für die Veröffentlichung von Daten. Von daher sind die Daten noch nicht in Linked Data verfügbar (JSON-LD, ...). Das würde eine kleine zusätzliche Aufgabe bedeuten, denn die Daten schon fast bereit sind. Aber wir konnten diese Aufgabe noch nicht priorisieren.
Wir haben uns nur vom Bibframe-Vokabular und -Struktur (und von einigen FOAF-Elementen für die Beschreibung von Personen) inspirieren lassen. Wir hatten keine Zeit, eine tiefere Analyse oder ein eigenes Vokabular zu entwickeln.
Einige Elemente respektieren Bibframe nicht, denn es war nicht genug präzis für eine Katalogisierungsarbeit und für eine RDA-Applikation (RDA war zu präzis für Bibframe). Beispiel: Veröffentlichungsangabe mit Ort/Verlag/Datum repetitiv und in einer bestimmten Reihenfolge ; Ausgabevermerk hat sehr viele veschiedene Elemente in RDA, und nur wenig in Bibframe.
Identifikatoren (Feld `identifiedBy`) ist das perfekte Beispiel für unsere Anwendung von Bibframe.
Bibframe 2.0 wird genutzt (noch nicht Bibframe 2.1.0).

Works und Instances in den Daten:
Die sind alle in einer einzigen Ressource "Document" zusammen, mit optionalem Link zur einer externalen Work-Autorität.
Wir müssten dieses Thema in einer ersten Phase leider aufgeben: die Katalogisierung (vor allem via Daten-Import) wäre zu komplex gewesen, eine gute Migration mit Generierung der Works wäre in der gegebenen Zeit unmöglich gewesen.

Migration:
Wir haben alle nützliche MARC-Daten migriert (d.h. extrem viele Felder), mit einem eigenen Skript und homemade Spezificationen. Das war seeehr anstrengend. Titel war das schlimmste Feld.
Das Mapping ist hier: https://docs.google.com/spreadsheets/d/1DCjCcMMylVsSoKIjsjx6ykWnGr9r_wPZ0fLRi3rlVpQ/edit?usp=sharing

Schulung der Bibliothekar*innen
Unser Erfassungsformular (Login: reroilstest@gmail.com / 123456) zeigt jetzt nur menschenlesbare Felder mit RDA Namen.
Die Katalogisierenden wissen nichts von Bibframe. Es wäre viel zu komplex, vor allem weil sehr kleine Bibliotheken in RERO+ arbeiten.
Die Arbeitsbasis ist jetzt RDA D-A-CH für die komplexe Katalogisierung. Für einfache Dinge verwenden sie die RERO ILS Dokumentation, wo sie Beispiele finden.
Bei den Schulungen war wichtig, immer die MARC-Äquivalenz anzugeben. Wir haben schon gehört, das die Ausbildung der neuen Personen/Lehrlinge einfacher mit RERO ILS als mit Virtua/MARC21 ist.

MARC-Interaktion
Es gibt eine Funktion in RERO ILS, um MARC-Daten aus verschiedenen Quellen zu importieren.
RERO ILS exportiert auch MARC21-Daten via SRU, zurzeit jedoch nur die allerwichtigsten Felder.

Einige Links, um die Daten anzuschauen

    Document im Katalog: https://bib.rero.ch/global/documents/1011252 (HTML-Daten für Menschen)
    Document in der API: https://bib.rero.ch/api/documents/1011252 (JSON/Bibframe-Daten, internes Format)
    Document via SRU (MARC21): https://bib.rero.ch/api/sru/documents?version=1.1&operation=searchRetrieve&query=pid=1011252

Alles ist ziemlich komplex, wenn man ausseralb des Themas und des Projekts ist. Ich bleibe zur Verfügung, falls du weitere Infos brauchst oder dir ein kurzes Meeting nützlich wäre.

Ist es das erste Mal, dass du diesen Kurs unterrichtest? Ich wünsche dir viel Erfolg!

Liebe Grüsse,

Nicolas





    Arbeitet ihr ausschliesslich mit Bibframe oder habt ihr noch Elemente aus anderen Vokabularen ergänzt? Oder ein eigenes Vokabular definiert?
    Werden Work und Instance bei der Katalogisierung einer Ressource erstellt? Und weitere Instances mit dem gleichen Work verknüpft?
    Wie seit ihr bei der Migration zurecht gekommen? Konntet ihr alle Daten aus MARC nach Bibframe migrieren? Hattet ihr dazu eigene Skripte oder verwendet ihr die Transformationen der Library of Congress?
    Welche Erfahrungen habt ihr bei der Schulung der Bibliothekar*innen für den Umstieg gemacht? Findet ein Umdenken von MARC zu Bibframe statt? Oder denken alle noch in MARC und "übersetzen" zu Bibframe?
    Wie geht ihr mit dem copy cataloguing um? Soweit ich sehe, habt ihr weiterhin MARC-Quellen, die genutzt werden können. Kann ich das Mapping dafür irgendwo sehen? Und bietet ihr eure Daten auch noch als MARC für die Fremddatenübernahme an?
    Wenn ich es richtig verstanden habe, arbeitet ihr intern mit JSON, das ja auch exportiert werden kann, und nicht mit JSON-LD oder einer anderen RDF-Serialisierung. Habe ich das richtig verstanden oder gibt es die Daten auch in einer RDF-Serialisierung?