---
title: Notizen
---

Musik
* 800 in creator https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/9972641472005504.jsonld, series fehlt
* 130 als creator https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/9972636951005504.jsonld, 830 fehlt, language nur 008 nicht 041

Monographie
* https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/9971158000105504.jsonld 830 fehlt, 655 enthalten, Link zu IHV fehlt

Allgemein
* Autoren im Label inkl. Jahreszahlen
* sameAs zu VIAF, als ID die query zur DNB???
* sind Rollen für creator/contributor relevant? -> eher nicht, 700 $edt als creator
* ausser @type Book keine Formatangaben
* 300 fehlt
* DOI fehlt
* 600 in creator


murmelgruppe:
* bisherige Erfahrungen? (Erstellen, Formate, ...)
* was möchten sie lernen? was mitnehmen?