---
title: "Übung"
---

Schauen Sie sich einige Aufnahmen für unterschiedliche Ressourcen (Bücher, Filme, Noten, ...) im MARC-Format über Alma oder swisscovery und im ExLibris JSONLD Format über die Alma Linked Data Schnittstelle an. Ziel ist es, sich mit der Schnittstelle zu beschäftigen und einen Eindruck über die darüber ausgelieferten Daten zu erhalten. Fragestellungen zur Anregung:
* Was fällt Ihnen zur Datentransformation auf?
* Wie beurteilen Sie Qualität und Vollständigkeit?
* Sehen Sie Probleme?
* Was würden Sie anders machen?
* Welche Vokabulare werden verwendet?

#### Mögliche Beispiele
* Monographie [MARC](https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma997210220105504&recordOwner=41SLSP_NETWORK), [JSONLD](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/997210220105504.jsonld)
* Monographie [MARC](https://uzb.swisscovery.slsp.ch/discovery/sourceRecord?vid=41SLSP_UZB:UZB&docId=alma990055958860205508&recordOwner=41SLSP_NETWORK), [JSONLD](https://open-na.hosted.exlibrisgroup.com/alma/41SLSP_UZB/bibs/990055958860205508.jsonld)
* Karte (mit Digitalisat auf e-rara) [MARC](https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma9926338650105504&recordOwner=41SLSP_NETWORK), [JSONLD](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/9926338650105504.jsonld)
* Noten [MARC](https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma9972636951005504&recordOwner=41SLSP_NETWORK), [JSONLD](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/9972636951005504.jsonld)
* Film [MARC](https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma9941304560105504&recordOwner=41SLSP_NETWORK), [JSONLD](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/9941304560105504.jsonld)
* Musikaufnahme [MARC](https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma9920305510105504&recordOwner=41SLSP_NETWORK), [JSONLD](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/9920305510105504.jsonld)
* Musikaufnahme [MARC](https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma9961222140105504&recordOwner=41SLSP_NETWORK), [JSONLD](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/9961222140105504.jsonld)
* Weitere nach eigenem Interesse

### Abfrage der Schnittstelle
* Aufbau einer Abfrage: https://<span>open-na.hosted.exlibrisgroup.com/alma/**institution code**/bibs/**mms id** (Achtung: IZ MMS ID!)
* Abfrage über die Schnittstelle der IZ Universität Zürich und ZB Zürich: https://open-na.hosted.exlibrisgroup.com/alma/41SLSP_UZB/bibs/990055958860205508.jsonld
* Abfrage über die Schnittstelle der IZ Region Basel: https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bibs/9971158000105504.jsonld

### Dokumentation und Hinweise
* [Dokumentation der Schnittstelle](https://developers.exlibrisgroup.com/alma/integrations/linked_data/jsonld/) im ExLibris Developers Network
* [JSON-LD Context]((https://open-na.hosted.exlibrisgroup.com/alma/contexts/bib)) des JSONLD von ExLibris
* Die [JSONLD Schnittstelle](https://developers.exlibrisgroup.com/alma/integrations/linked_data/jsonld/) muss in Alma von der Institution (in SLSP von der Institution Zone) freigeschaltet werden.