---
title: "BIBFRAME Transformationen"
date: 2023-02-27T17:28:11+02:00
draft: true
---

### Aufgabe

Wählen Sie sich eine oder mehrere MARC Aufnahmen als Beispiele aus und schauen Sie sich daraus für einen (oder mehrere) Bereiche die Transformation zu BIBFRAME an. Ziel ist es, sich mit BIBFRAME zu beschäftigen und zu sehen, wie MARC Daten zu BIBFRAME transformiert werden können.

Dazu können Sie...

* ... versuchen, den gewählten Bereich selbst in BIBFRAME abzubilden. Basis dafür ist die [BIBFRAME Ontology](https://id.loc.gov/ontologies/bibframe-category.html).
* ... sich den gewählten Bereich in den [Spezifikationen](https://www.loc.gov/bibframe/mtbf/) für die Transformation von MARC zu BIBFRAME der Library of Congress anschauen. Anhand der Spezifikationen können Sie auch eine eigene Transformation prüfen.
* ... in Alma oder über die BIBFRAME Schnittstelle von Alma die Aufnahme in MARC und BIBFRAME vergleichen. 

#### Mögliche Beispiele
* Titel (Felder 245, 246 ; Beispiel [MARC](https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma9920305510105504&recordOwner=41SLSP_NETWORK), [BIBFRAME](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bf/entity/instance/9920305510105504))
* Erscheinungsangaben (264 ; Beispiel [MARC](https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma9961222140105504&recordOwner=41SLSP_NETWORK), [BIBFRAME](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bf/entity/instance/9961222140105504))
* Übergeordnete Aufnahmen (490, 8xx ; Beispiel [MARC](https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma997210220105504&recordOwner=41SLSP_NETWORK), [BIBFRAME](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bf/entity/instance/997210220105504))
* Weitere nach eigenem Interesse

#### BIBFRAME in Alma

Basis für die Transformation in Alma sind die Skripte der Library of Congress.

In Alma kann bei der Detailansicht einer Aufnahme zwischen MARC und BIBFRAME umgeschaltet werden.

Die [BIBFRAME Schnittstelle](https://developers.exlibrisgroup.com/alma/integrations/linked_data/BIBFRAME/) muss von der Institution (in SLSP von der IZ) freigeschaltet werden.



[Beispiel aus der IZ Region Basel](https://open-eu.hosted.exlibrisgroup.com/alma/41SLSP_UBS/bf/entity/instance/9962421190105504)