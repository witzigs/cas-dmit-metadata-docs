---
title: "IMD-Typen"
draft: true
---

Gekürzter Auszug aus IDS Erfassungsleitfaden RDA (Stand Oktober 2020)

## 336    Inhaltstyp (R)

Indikatoren:  _ _

Unterfeldcodes

        $a   Inhaltstyp - Begriff (R)
        $b   Inhaltstyp - Code (R)
        $2   Quelle (NR)

### Inhalt

Dieses Feld enthält die Kommunikationsform, in welcher das Werk veröffentlicht wurde. Das Feld 
wird zusammen mit dem Leader Pos. 06 (Materialtyp) genutzt, welches den allgemeinen Inhaltstyp angibt. 
Das Feld 336 ermöglicht es, genauere Angaben zum Inhaltstyp zu erfassen. 

### Erfassung

Das Unterfeld $a wird zusammen mit Unterfeld $b und Unterfeld $2 rdacontent genutzt. 

### Für die Übung relevante Werte

        336 __ $a Noten $b ntm $2 rdacontent
        336 __ $a Text $b txt $2 rdacontent

## 337    Medientyp (R)

Indikatoren:  _ _

Unterfeldcodes

        $a   Medientyp - Begriff (R)
        $b   Medientyp - Code (R)
        $2   Quelle (NR)
 

### Inhalt

Dieses Feld beinhaltet die allgemeinen Informationen zur Art des Geräts, welches benötigt wird um den 
Inhalt einer Ressource abzuspielen. Bei gewissen Medientypen muss zusätzlich der Medientyp in 
Feld 007 kodiert werden. Das Feld 337 ermöglicht es, genauere Angaben zum Medientyp zu erfassen.

### Erfassung

Das Unterfeld $a wird zusammen mit Unterfeld $b und Unterfeld $2 rdamedia genutzt.

### Für die Übung relevante Werte

        337 __ $a ohne Hilfsmittel zu benutzen $b n $2 rdamedia
        337 __ $a Mikroform $b h $2 rdamedia
        337 __ $a Computermedien $b c $2 rdamedia

## 338    Datenträgertyp (R)

Indikatoren:  _ _

Unterfeldcodes

        $a   Datenträgertyp - Begriff (R)
        $b   Datenträgertyp - Code (R)
        $2   Quelle (NR)

### Inhalt

Dieses Feld enthält die Angaben zum Format des Speichermediums oder zum Gehäuse eines Datenträgers in Kombination
mit der Art des Geräts, das benötigt wird, um den Inhalt einer Ressource anzuschauen, abzuspielen oder 
laufen zu lassen. Das Feld 338 ermöglicht es, genauere Angaben zum spezifischen Datenträgertyp 
zu erfassen. Feld 338 wird immer abhängig von Feld 337 vergeben.

### Erfassung

Das Unterfeld $a wird zusammen mit Unterfeld $b und Unterfeld $2 rdacarrier genutzt.

### Für die Übung relevante Werte

        338 __ $a Band $b nc $2 rdacarrier (bei 337 ohne Hilfsmittel zu benutzen)
        338 __ $a Mikrofiche $b he $2 rdacarrier (bei 337 Mikroform)
        338 __ $a Mikrofilmspule $b hd $2 rdacarrier (bei 337 Mikroform)
        338 __ $a Mikrofilmstreifen $b hh $2 rdacarrier (bei 337 Mikroform)
        338 __ $a Online-Ressource $b cr $2 rdacarrier (bei 337 Computermedien)