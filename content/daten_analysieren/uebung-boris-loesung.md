---
title: "Übung BORIS - Lösungen"
weight: "23"
---

### Lösungen
<br/>
<details>
    <summary>Wie viele Aufnahmen sind enthalten?</summary>
      <ul>
        <li>22924 Aufnahmen</li>
        <li>Auszählung mit grep über den record tag</li>
        <li>grep -c '&lt;record&gt;' boris-export.20201127103438.xml</li>
      </ul>
</details>
<br/>
<details>
    <summary>Wie oft kommen Sprachangaben vor?</summary>
      <ul>
        <li>23837 Felder dc:language</li>
        <li>Auszählung mit grep über dc:language</li>
        <li>grep -c 'dc:language' boris-export.20201127103438.xml</li>
      </ul>
</details>
<br/>
<details>
    <summary>Welche Sprachcodes kommen vor und welche wie oft?</summary>
    <div>
        5 arg<br/>
        3209 deu<br/>
        20056 eng<br/>
        297 fra<br/>
        2 hun<br/>
        32 ita<br/>
        2 lao<br/>
        115 me<br/>
        2 pol<br/>
        13 por<br/>
        5 rus<br/>
        1 sh<br/>
        96 spa<br/>
        1 tgk<br/>
        1 zho
   </div>
   <ul>
        <li>grep 'dc:language' boris-export.20201127103438.xml | cut -c 22-24 | sort | uniq -c </li>
   </ul>
</details>
<br/>
<details>
    <summary>Welche identifier sind in den Daten enthalten?</summary>
      <ul>
        <li>DOI, PMID, ISSN, BORIS URL, String mit Zitat</li>
        <li>Auswertung des Felds dc:identifier</li>
      </ul>
</details>
<br/>
<details>
    <summary>Wie kann die PMID erkannt werden?</summary>
      <ul>
        <li>Durch den prefix info:pmid im Feld dc:identifier</li>
      </ul>
</details>
<br/>
<details>
    <summary>Erstelle eine Liste aller PMIDs.</summary>
    <ul>
        <li>grep 'dc:identifier&gt;info:pmid' boris-export.20201127103438.xml > boris-pmid.csv</li>
    </ul>
</details>


### Lösungen zu den optionalen Fragen
<br/>
<details>
    <summary>Gibt es PMIDs, die doppelt vorhanden sind?</summary>
      <ul>
        <li>Ja, PMID 31536527 kommt zwei Mal in den Daten vor.</li>
        <li>sort boris-pmid.csv | uniq -d</li>
      </ul>
</details>
<br/>
<details>
    <summary>Wie sind die Sprachen codiert?</summary>
      <ul>
        <li>Mit einem Code aus 2 oder 3 Buchstaben, wahrscheinlich ISO-639-3 (deu vs ger)</li>
      </ul>
</details>
<br/>
<details>
    <summary>Sind diese Sprachcodes alle gültig? Wofür steht der Code "me"?</summary>
      <ul>
        <li>Nein, die Codes sh und me sind nach ISO-639-3 nicht gültig.</li>
        <li>me wird für "Mehrere Sprachen" verwendet. Das lässt sich an einem Beispiel auf der Oberfläche feststellen, z.B.: https://boris.unibe.ch/2132/</li>
      </ul>
</details>
<br/>
<details>
    <summary>Analysieren Sie das Feld dcterms:bibliographicCitation in denen das Wort journal (case insensitive) vorkommt? Welche Informationen sind darin enthalten? Wie sind diese Informationen voneinander abgetrennt?</summary>
      <ul>
        <li>Zeitschriftentitel, Volumenummer(Issuenummer), Seitenzahlen (eingeleitet mit pp. oder p.). Verlag</li>
        <li>Zeitschriftentitel, Volumenummer(Issuenummer), Seitenzahlen (eingeleitet mit pp. oder p.). Ort: Verlag</li>
        <li>Zeitschriftentitel, Volumenummer, Seitenzahlen (eingeleitet mit pp. oder p.). Ort: Verlag</li>
        <li>grep -i 'dcterms:bibliographicCitation' boris-export.20201127103438.xml</li>
      </ul>
</details>
<br/>
<details>
    <summary>Die Informationen aus dcterms:bibliographicCitation sollen ins MARC-Feld 773 übertragen werden. Überlegen Sie sich Regeln nach denen dies möglich wäre und verifizieren Sie diese anhand der Daten.</summary>
      <ul>
        <li>Als Trennzeichen zwischen Titel und Inhalten für 773 $g: Komma Spatium Zahl</li>
        <li>Als Trennzeichen zwischen Seitenzahlen und Ort/Verlag: Zahl Punkt Spatium</li>
      </ul>
</details>
<br/>
<details>
    <summary>Aus welchen Feldern können die für 773 noch zusätzlich notwendigen Informationen extrahiert werden?</summary>
      <ul>
        <li>Die ISSN aus dc:identifier für 773 $x</li>
        <li>Das Erscheinungsjahr aus dc:date für 773 $g</li>
      </ul>
</details>

