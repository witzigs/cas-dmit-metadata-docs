---
title: "Übung Mikrofilmarchiv"
date: 2022-02-18T13:58:11+02:00
draft: true
---

### Datenset "Mikrofilmarchiv Musik"
Kontext: Die Daten stammen aus einer selbst entwickelten relationalen Datenbank und liegen in einem nicht dokumentierten Format vor. Die Erfassungsregeln orientieren sich ebenfalls nicht an einem bekannten Standard. Enthalten sind im Mikrofilmarchiv neben Mikrofilmen u.ä. auch Fotokopien und PDFs, einerseits für Musikdrucke und andererseits für Werke zur Musik. Nun sollen die Daten nach MARC21 (Bibliographic) transformiert und ins Bibliotheksverwaltungssystem importiert werden. Dazu müssen die Daten analysiert und entsprechend vorbereitet werden.
Die Datei mikrofilmarchiv.csv enthält einen Export im CSV-Format der Datenbank.

#### Fragen
* Welche Informationen zu physischen Formaten (Mikrofilm, Partitur, Fotokopie, etc.) sind enthalten?
* Haben wir genügend Informationen um einen korrekten IMD-Block nach RDA (Inhalts-, Medien-, Datenträgertyp in Feld 336/337/338 ; [Hilfestellung für die Übung](/daten_analysieren/cmd)) zu erstellen?
* Analysieren Sie die Spalte Medium. Wie viele Einträge gibt es pro Medium?
* Erstellen Sie eine neue Spalte mit der vollständigen Signatur. Die Signatur soll einen zusätzlichen Prefix "SCB" erhalten.
* Analysieren Sie die Spalte Autor. Kommen mehrere Personen vor? Wenn ja, welche Zeichen werden als Trennzeichen zwischen verschiedenen Personen verwendet? 
* Trennen Sie mehrere Autoren in unterschiedliche Spalten auf.
* Ein Teil der Aufnahmen wurde bereits manuell im Bibliotheksverwaltungssystem erfasst, jedoch wurden die Werte aus den Spalten keyword und gattung dabei nicht erfasst. Diese Angaben sollen nun ergänzt werden. Sie haben eine Liste der Exemplare mit Signatur Prefix SCB Mf_ erhalten, die bereits im Bibliotheksverwaltungssystem erfasst sind (Datei mikrofilm-archiv-bereits-erfasst.csv). Ergänzen Sie diese Liste um die Spalten keyword und gattung.

#### Tipps
<br/>
<details>
  <summary>Zu den Informationen zu Formaten</summary>
  <ul>
    <li>Schauen Sie sich die Spalten medium, kategorie, praes_form und filmart genauer an.</li>
    <li>Ja, die Informationen sind zum Teil nicht einfach zuzuordnen.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Zur Analyse von medium</summary>
  <ul>
    <li>Die Excel-Funktion ZÄHLENWENN ist nützlich.</li>
  </ul>  
</details>
<br/>
<details>
  <summary>Zum Erstellen der vollständigen Signatur</summary>
  <ul>
    <li>Die Excel-Funktion VERKETTEN hilft hier weiter.</li>
  </ul>  
</details>
<br/>
<details>
  <summary>Zum Abtrennen der Autoren</summary>
  <ul>
    <li>Dazu ist der Befehl "Text in Spalten" nützlich.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Zum Abgleich mit den bereits erfassten Exemplaren</summary>
  <ul>
    <li>Schauen Sie sich die Excel-Funktion SVERWEIS an.</li>
    <li>Wichtig ist, dass die Signaturen indentisch sind.</li>
  </ul>
</details>

#### Lösungen
<br/>
<details>
  <summary>Welche Informationen zu physischen Formaten (Mikrofilm, Partitur, Fotokopie, etc.) sind enthalten?</summary>
  <ul>
    <li>In medium: Mikrofilm, Streifenfilm, Digital, Fotokopie und weiter unklare Angaben</li>
    <li>In kategorie: Druck, Handschrift</li>
    <li>In praes_form: Partitur, Stimmen, Tabulatur, ...</li>
    <li>In filmart: negativ, Fotokopie</li>
    <li>Teilweise finden sich im Titel oder der Spalte keywords weitere Angaben.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Haben wir genügend Informationen um einen korrekten IMD-Block nach RDA zu erstellen?</summary>
  <ul>
    <li>336: Nein, unklar was Noten und was Text ist</li>
    <li>337: Teilweise. Über die Angaben in der Spalte medium kann Fotokopie zu ohne Hilfsmittel, Mikrofilm zu Mikroform und Digital zu Computermedien zugeordnet werden.</li>
    <li>338: Nein, für Mikrofilm. Ja, für ohne Hilfsmittel und Computermedien da dafür Standardwerte vergeben werden können.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Analysieren Sie die Spalte Medium. Wie viele Einträge gibt es pro Medium?</summary>
  <div>
  mikrofilm:	3485<br/>
  keinesignatur:	41<br/>
  fotokopie:	217<br/>
  fiche:	79<br/>
  wenzingerslg:	291<br/>
  wernlislg:	12<br/>
  digital:	120<br/>
  streifenfilm:	2<br/>
  giegling:	329<br/>
  </div>
</details>
<br/>
<details>
  <summary>Analysieren Sie die Spalte Autor. Kommen mehrere Personen vor? Wenn ja, welche Zeichen werden als Trennzeichen zwischen verschiedenen Personen verwendet? </summary>
  <ul>
    <li>Ja, zum Teil werden mehrere Personen genannt. Leider wurden Trennzeichen nicht konsistent verwendet.</li>
    <li> / und ; kommen als Trennzeichen vor.</li>
    <li>[] werden teilweise für Editoren und Pseudonyme verwendet.</li>
  </ul>
</details>