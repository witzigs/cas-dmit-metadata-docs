---
title: "Übung BORIS"
weight: "21"
---

### Kontext
Für die Übung steht ein Datenset aus BORIS auf Teams zur Verfügung.

Das Datenset wurde über die OAI-Schnittstelle von BORIS in Dublin Core exportiert. Die Daten sollen für ein Projekt weiter genutzt werden und müssen dazu in MARC21 (Bibliographic) umgewandelt werden. Dazu müssen die Daten zunächst analysiert werden.

Sie können die Daten mit einem Tool Ihrer Wahl analysieren. Mein Vorschlag ist die Verwendung von Shell-Befehlen. Tipps und Lösungen beziehen sich darauf.

Nehmen Sie sich für die Übung 30 Minuten Zeit und schauen Sie, wie weit Sie kommen. Es müssen nicht alle Fragen beantwortet werden!

#### Fragen zu den Daten
* Wie viele Aufnahmen sind enthalten?
* Wie oft kommen Sprachangaben vor?
* Welche Sprachcodes kommen vor und welche wie oft?
* Welche Identifier sind in den Daten enthalten?
* Wie kann die PMID (PubMed ID) erkannt werden?
* Erstelle eine Liste aller PMIDs.

#### Optionale Fragen zu den Daten
* Gibt es PMIDs, die doppelt vorhanden sind?
* Wie sind die Sprachen codiert?
* Sind die vorkommenden Sprachcodes alle gültig? Wofür steht der Code "me"?
* Analysieren Sie die Felder dcterms:bibliographicCitation in denen das Wort journal (case insensitive) vorkommt. Welche Informationen sind darin enthalten? Wie sind diese Informationen voneinander abgetrennt?
* Die Informationen aus dcterms:bibliographicCitation sollen ins MARC-Feld 773 ([Hilfestellung für die Übung](/daten_analysieren/773)) übertragen werden. Überlegen Sie sich Regeln nach denen dies möglich wäre und verifizieren Sie diese anhand der Daten.
* Aus welchen Feldern können die für 773 noch zusätzlich notwendigen Informationen extrahiert werden?



#### Tipps
<br/>
<details>
  <summary>Zum Zählen</summary>
  <ul>
    <li>grep hat eine nützliche Option -c. Eine Verkettung von grep und wc -l funktioniert ebenfalls.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Zur Auswertung der Sprachcodes</summary>
  <ul>
    <li>Hier ist eine Verkettung von Shell-Befehlen notwendig. Schauen Sie sich grep, cut mit Option -c, sort und uniq mit Option -c an.</li>
  </ul>  
</details>
<br/>
<details>
  <summary>Zum Erstellen der PMID-Liste</summary>
  <ul>
    <li>Hier kann mit grep und cut oder einer Kombination von grep und sed gearbeitet werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Zum Finden der doppelten PMIDs</summary>
  <ul>
    <li>Hier ist ebenfalls uniq nützlich, aber mit Option -d</li>
  </ul>
</details>
<br/>
<details>
  <summary>Zur Analyse von dcterms:bibliographicCitation</summary>
  <ul>
    <li>grep hat eine Option -i für case insensitive</li>
  </ul>
</details>

