---
title: "Requests und Arguments"
date: 2021-02-13T13:26:46+02:00
draft: true
---

### Datenset aus BORIS in Dublin Core
* Wie viele Aufnahmen sind enthalten?
zgrep -c '<record>' boris-export.20201127103438.xml.gz 
22924
* Wie oft kommen Sprachangaben vor? 
zgrep -c 'dc:language' boris-export.20201127103438.xml.gz 
23837
* Wie sind die Sprachen codiert?
 Code aus 2 oder 3 Buchstaben, wahrscheinlich ISO-639-3 (deu vs ger)
* Welche Sprachen kommen vor und welche wie oft?
zgrep 'dc:language' boris-export.20201127103438.xml.gz  | cut -c 22-24 | sort | uniq -c 
      5 arg
   3209 deu
  20056 eng
    297 fra
      2 hun
     32 ita
      2 lao
    115 me<
      2 pol
     13 por
      5 rus
      1 sh<
     96 spa
      1 tgk
      1 zho
* Sind diese Sprachcodes alle gültig? Wofür steht der Code "me"?
less boris, darin suchen: /dc:language.*me<\/dc:lan
Beispiel anschauen: https://boris.unibe.ch/2132/
Mehrere Sprachen
* Welche identifier sind in den Daten enthalten?
zgrep 'dc:identifier' boris-export.20201127103438.xml.gz
DOI, PMID, ISSN, BORIS URL, String mit Zitat
* Wie kann die PMID erkannt werden?
Durch den prefix info:pmid im Feld dc:identifier
* Erstelle eine Liste aller PMIDs
zgrep 'dc:identifier>info:pmid' boris-export.20201127103438.xml.gz | sed 's/        <dc:identifier>info:pmid://g' | sed 's/<\/dc:identifier>//g' > boris-pmid.csv
* Gibt es PMIDs, die doppelt vorhanden sind?
sort boris-pmid.csv | uniq -c | sort -rh | less
sort boris-pmid.csv | uniq -d
Ja: 31536527

* Analysieren Sie das Feld dcterms:bibliographicCitation in denen das Wort journal (case insensitive) vorkommt? Welche Informationen sind darin enthalten? Wie sind diese Informationen voneinander abgetrennt?
Zeitschriftentitel, Volumenummer(Issuenummer), Seitenzahlen (eingeleitet mit pp. oder p.). Verlag
Zeitschriftentitel, Volumenummer(Issuenummer), Seitenzahlen (eingeleitet mit pp. oder p.). Ort: Verlag
Zeitschriftentitel, Volumenummer, Seitenzahlen (eingeleitet mit pp. oder p.). Ort: Verlag
Zeitschriftentitel zum Teil mit Reihe

* Die Informationen aus dcterms:bibliographicCitation sollen ins MARC-Feld 773 übertragen werden. Überlegen Sie sich Regeln nach denen dies möglich wäre und verifizieren Sie diese anhand der Daten.
Komma Spatium Zahl -> Trenner für Titel und $g
Zahl Punkt Spatium -> Trenner für Ort und Verlag 

* Aus welchen Feldern können die für 773 noch zusätzlich notwendigen Informationen extrahiert werden?
dc:identifer issn
dc:date

-----

### Datenset Mikrofilmarchiv SCB
* Welche Informationen zu Formaten sind enthalten?
Filtern
medium (Mikrofilm, Streifenfilm, Digital, Fotokopie und unverständliche Angaben)
kategorie (Druck, Handschrift)
praes_form (Partitur, Stimmen, Tabulatur...)
filmart (negativ, Fotokopie)
Dann auch noch teilweise in Titel oder Keywords

* Haben wir genügend Informationen um einen korrekten RDA content/media/carrier (336/337/338) zu erstellen?
336: Nein, unklar was Noten und was Text ist
337: Tlw., über Medium kann Fotokopie -> ohne Hilfsmittel, Mikrofilm -> Mikroform, Digital -> Computermedien zugeordnet werden
338: Nein, für Mikrofilm. Ja, für ohne Hilfsmittel und Computermedien da Standardwerte

* Wie viele Einträge gibt es pro Medium
Liste Medium kopieren, Duplikate entfernen, als Input für ZÄHLENWENN benutzen

* Die Signatur ist im Auszug in zwei Spalten, getrennt nach Prefix und Signatur, enthalten. Erstellen Sie eine neue Spalte mit der vollständigen Signatur. Die Signatur soll einen zusätzlichen Prefix SCB erhalten.
Neue Spalte erstellen, darin VERKETTEN('SCB';spalte prefix;spalte signatur)

* Analysieren Sie die Spalte Autor. Welche Zeichen werden verwendet? Kommen mehrere Personen vor?
Chaotisch: / ; [] für Editoren, Pseudonyme

* Trennen Sie mehrere Autoren in unterschiedliche Spalten auf
Neue Spalten erstellen
Text in Spalten auf /

* Ein Teil der Aufnahmen wurde bereits manuell im Bibliotheksverwaltungssystem erfasst, jedoch wurden keyword und gattung dabei nicht erfasst. Diese Angaben sollen nun ergänzt werden. Sie haben eine Liste der Exemplare mit Signatur Prefix SCB Mf- erhalten. Ergänzen Sie diese Liste um die Spalten keyword und gattung.
SVERWEIS
(Achtung auf Gi- Signaturen... Leerzeichen)

