---
title: "MODS"
date: 2020-09-22T20:30:12+02:00
---
https://www.loc.gov/standards/mods/
* Metadata object description standard
* Kompromiss zwischen MARC (komplex) und Dublin Core (oft zu einfach)
* XML, tags in englisch (statt Zahlen von MARC)
* hat aber auch Elemente, die in MARC nicht vorhanden sind

### Anwendungsbeispiele
* Im Kontext von Repositories (vgl. METS, e-rara, etc.)
* Auch OA-Repositories (serval)
* Austauschformat (JISC copac https://discover.libraryhub.jisc.ac.uk/support/api/ -> SRU und JSON als Schnittstellen Beispiele!)

