---
title: "METS"
date: 2020-09-22T20:30:24+02:00
---

http://www.loc.gov/standards/mets/
* Metadata encoding and transmission standard
* standard for packaging descriptive, administrative and structural metadata in one XML document (for interactive exchange with digital repositories). Contanis hierarchical structure of digital library objects, names and locations of the files that constitute those objects, all associated metadata (in various formats, MODS aber auch MARC oder anderes)

### Struktur
* METS Header: metadata about the METS file
* Descriptive Metadata: descriptive metadata records in any format or pointing to it
* Administrative Metadata: technical, IP rights, analog source, provenance, ...
* File Section: inventory of the content files
* Structural Map: hierarchical description of the object’s physical and/or logical structure
* Structural Links: metadata recording the existence of links in the object
* Behaviors: AIP and information for any software behaviors needed to work with the object or it’s metadata

### Anwendungsbeispiele

* Strukturierung von digitalen Objekten
* verwendet für e-rara https://arbido.ch/de/ausgaben-artikel/2011/elektronische-bibliothek-schweiz/e-rara-ch-ein-schweizer-digitalisierungsprojekt-mit-internationaler-ausstrahlung
* verwendet für Migration Aleph -> Alma

