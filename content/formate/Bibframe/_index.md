---
title: "Bibframe"
date: 2022-02-22T20:31:01+02:00
---

* https://zenodo.org/record/3904123#.X0dSPRlCTQ8
* https://www.loc.gov/bibframe/ (XSLT MARC21 to Bibframe und umgekehrt, Comparison viewer)
* 
* https://www.mynewsdesk.com/se/kungliga_biblioteket/pressreleases/kb-becomes-the-first-national-library-to-fully-transition-to-linked-data-2573975
* https://libris.kb.se/katalogisering/help/workflow-print-monograph
* https://id.kb.se/doc/model
* https://id.kb.se/vocab/
* https://docs.google.com/presentation/d/e/2PACX-1vQF2ek2i3-6spsiMjMcSi03J4Nm4CdRvsupkeQ826w7Ctl4QO4iftttAeGx_dPUpJEhQJ3brCjCVWRZ/pub?start=false&loop=false&delayms=3000#slide=id.g57212a014a_0_1776
