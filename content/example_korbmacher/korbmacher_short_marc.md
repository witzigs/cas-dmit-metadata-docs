---
title: "MARC21, MARCXML"
weight: 10
---

{{< highlight go-html-template "lineNos=inline" >}}

<record xmlns="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.loc.gov/MARC21/slim http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd">
    <leader>04289nam a2200973 c 4500</leader>
    <controlfield tag="001">9931231890105504</controlfield>
    <controlfield tag="008">201012s2004    sz            00| | ger  </controlfield>
    <datafield tag="020" ind1=" " ind2=" ">
        <subfield code="a">3905342227</subfield>
    </datafield>
    <datafield tag="035" ind1=" " ind2=" ">
        <subfield code="a">(IDSBB)003123189DSV01</subfield>
    </datafield>
    <datafield tag="040" ind1=" " ind2=" ">
        <subfield code="a">SzZuIDS BS/BE A258</subfield>
        <subfield code="e">kids</subfield>
        <subfield code="d">CH-ZuSLS</subfield>
    </datafield>
    <datafield tag="041" ind1="0" ind2=" ">
        <subfield code="a">ger</subfield>
        <subfield code="a">roh</subfield>
    </datafield>
    <datafield tag="082" ind1="1" ind2="4">
        <subfield code="a">390</subfield>
        <subfield code="2">15</subfield>
    </datafield>
    <datafield tag="084" ind1=" " ind2=" ">
        <subfield code="a">LB 94150</subfield>
        <subfield code="2">rvk</subfield>
    </datafield>
    <datafield tag="100" ind1="1" ind2=" ">
        <subfield code="a">Maissen, Alfons</subfield>
        <subfield code="d">1905-2003</subfield>
        <subfield code="0">(DE-588)10148318X</subfield>
    </datafield>
    <datafield tag="245" ind1="1" ind2="0">
        <subfield code="a">&lt;&lt;Der&gt;&gt; Korbmacher</subfield>
        <subfield code="b">= Il canistrer : (1942)</subfield>
        <subfield code="c">Alfons Maissen, Anna-Pia Maissen ; [Verein für Bündner Kulturforschung und
            Museum Regiunal Surselva]
        </subfield>
    </datafield>
    <datafield tag="246" ind1="1" ind2="1">
        <subfield code="a">&lt;&lt;Il&gt;&gt; canistrer</subfield>
    </datafield>
    <datafield tag="264" ind1=" " ind2="1">
        <subfield code="a">Chur</subfield>
        <subfield code="b">Bündner Monatsblatt</subfield>
        <subfield code="c">2004</subfield>
    </datafield>
    <datafield tag="300" ind1=" " ind2=" ">
        <subfield code="a">50 S.</subfield>
        <subfield code="b">Ill.</subfield>
        <subfield code="c">25 cm</subfield>
    </datafield>
    <datafield tag="336" ind1=" " ind2=" ">
        <subfield code="b">txt</subfield>
        <subfield code="2">rdacontent</subfield>
    </datafield>
    <datafield tag="337" ind1=" " ind2=" ">
        <subfield code="b">n</subfield>
        <subfield code="2">rdamedia</subfield>
    </datafield>
    <datafield tag="338" ind1=" " ind2=" ">
        <subfield code="b">nc</subfield>
        <subfield code="2">rdacarrier</subfield>
    </datafield>
    <datafield tag="490" ind1="0" ind2=" ">
        <subfield code="a">Reihe Altes Handwerk</subfield>
        <subfield code="v">Heft 65</subfield>
    </datafield>
    <datafield tag="500" ind1=" " ind2=" ">
        <subfield code="a">Teilw. deutscher und rätoromanischer Paralleltext</subfield>
    </datafield>
    <datafield tag="600" ind1="1" ind2="7">
        <subfield code="a">Maissen, Alfons</subfield>
        <subfield code="2">idsbb</subfield>
    </datafield>
    <datafield tag="648" ind1=" " ind2="7">
        <subfield code="a">Geschichte 1942</subfield>
        <subfield code="2">gnd</subfield>
    </datafield>
    <datafield tag="650" ind1=" " ind2="7">
        <subfield code="a">Holzhandwerk</subfield>
        <subfield code="0">(DE-STW)13109-6</subfield>
        <subfield code="2">stw</subfield>
    </datafield>
    <datafield tag="650" ind1=" " ind2="7">
        <subfield code="a">Möbelindustrie</subfield>
        <subfield code="0">(DE-STW)12921-3</subfield>
        <subfield code="2">stw</subfield>
    </datafield>
    <datafield tag="650" ind1=" " ind2="7">
        <subfield code="a">Vannerie</subfield>
        <subfield code="0">(IDREF)027255468</subfield>
        <subfield code="2">idref</subfield>
    </datafield>
    <datafield tag="650" ind1=" " ind2="7">
        <subfield code="a">Korbmacher</subfield>
        <subfield code="0">(DE-588)4165301-4</subfield>
        <subfield code="2">gnd</subfield>
    </datafield>
    <datafield tag="651" ind1=" " ind2="7">
        <subfield code="a">Graubünden</subfield>
        <subfield code="0">(DE-588)4021881-8</subfield>
        <subfield code="2">gnd</subfield>
    </datafield>
    <datafield tag="651" ind1=" " ind2="7">
        <subfield code="a">Vorderrhein (Tal, Graubünden, Schweiz)</subfield>
        <subfield code="2">idszbz</subfield>
    </datafield>
    <datafield tag="655" ind1=" " ind2="7">
        <subfield code="a">Bildband</subfield>
        <subfield code="2">gnd-content</subfield>
    </datafield>
    <datafield tag="690" ind1=" " ind2=" ">
        <subfield code="e">338.4</subfield>
        <subfield code="a">Handwerkswirtschaft und Handwerkspolitik</subfield>
        <subfield code="2">hsg-kmu</subfield>
    </datafield>
    <datafield tag="700" ind1="1" ind2=" ">
        <subfield code="a">Maissen, Anna Pia</subfield>
        <subfield code="d">1957-</subfield>
        <subfield code="0">(DE-588)118176773</subfield>
    </datafield>
    <datafield tag="710" ind1="2" ind2=" ">
        <subfield code="a">Verein für Bündner Kulturforschung</subfield>
        <subfield code="0">(DE-588)2143724-5</subfield>
    </datafield>
    <datafield tag="830" ind1=" " ind2="0">
        <subfield code="a">Altes Handwerk</subfield>
        <subfield code="v">65</subfield>
        <subfield code="w">991007902339705501</subfield>
    </datafield>
    <datafield tag="990" ind1=" " ind2=" ">
        <subfield code="f">BIBsgv</subfield>
        <subfield code="9">LOCAL</subfield>
    </datafield>
</record>
{{< / highlight >}}