---
title: "Metadatenstandards - Beispiel"
weight: 35
---

Das verwendete Beispiel basiert auf der Aufnahme [991058291859705501](https://swisscovery.slsp.ch/permalink/41SLSP_NETWORK/1ufb5t2/alma991058291859705501) aus SLSP, wurde aber stark gekürzt und leicht vereinfachet.