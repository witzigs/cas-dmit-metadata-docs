---
title: "MODS, XML"
weight: 30
---

MODS wurde mittels des [XSLT Stylesheets MARCXML to MODS 3.8](https://www.loc.gov/standards/mods/v3/MARC21slim_MODS3-8_XSLT2-0.xsl) der Library of Congress erstellt.

{{< highlight go-html-template "lineNos=inline" >}}

<?xml version="1.0" encoding="UTF-8"?>
<mods xmlns="http://www.loc.gov/mods/v3"
       xmlns:local="http://www.loc.org/namespace"
       xmlns:xs="http://www.w3.org/2001/XMLSchema"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       version="3.8"
       xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-8.xsd">
   <titleInfo>
      <title>&lt;&lt;Der&gt;&gt; Korbmacher</title>
      <subTitle>= Il canistrer : (1942)</subTitle>
   </titleInfo>
   <titleInfo type="translated">
      <title>&lt;&lt;Il&gt;&gt; canistrer</title>
   </titleInfo>
   <name type="personal" usage="primary">
      <namePart>Maissen, Alfons</namePart>
      <namePart type="date">1905-2003</namePart>
   </name>
   <name type="personal">
      <namePart>Maissen, Anna Pia</namePart>
      <namePart type="date">1957-</namePart>
   </name>
   <name type="corporate">
      <namePart>Verein für Bündner Kulturforschung</namePart>
   </name>
   <typeOfResource>text</typeOfResource>
   <genre authority="rdacontent"/>
   <genre authority="gnd-content">Bildband</genre>
   <originInfo>
      <place>
         <placeTerm type="code" authority="marccountry">sz</placeTerm>
      </place>
      <dateIssued encoding="marc">2004</dateIssued>
      <issuance>monographic</issuance>
   </originInfo>
   <originInfo eventType="publication">
      <place>
         <placeTerm type="text">Chur</placeTerm>
      </place>
      <agent>
         <namePart>Bündner Monatsblatt</namePart>
         <role>
            <roleTerm>publisher</roleTerm>
         </role>
      </agent>
      <dateIssued>2004</dateIssued>
   </originInfo>
   <language>
      <languageTerm authority="iso639-2b" type="code">ger</languageTerm>
   </language>
   <language>
      <languageTerm authority="iso639-2b" type="code">roh</languageTerm>
   </language>
   <physicalDescription>
      <form authority="marcform">print</form>
      <extent>50 S. Ill. 25 cm</extent>
   </physicalDescription>
   <note type="statement of responsibility">Alfons Maissen, Anna-Pia Maissen ; [Verein für Bündner Kulturforschung und
            Museum Regiunal Surselva]
        </note>
   <note>Teilw. deutscher und rätoromanischer Paralleltext</note>
   <subject authority="idsbb">
      <name type="personal">
         <namePart>Maissen, Alfons</namePart>
      </name>
   </subject>
   <subject authority="gnd">
      <temporal>Geschichte 1942</temporal>
   </subject>
   <subject xmlns:xlink="http://www.w3.org/1999/xlink"
             authority="stw"
             xlink:href="(DE-STW)13109-6">
      <topic>Holzhandwerk</topic>
   </subject>
   <subject xmlns:xlink="http://www.w3.org/1999/xlink"
             authority="stw"
             xlink:href="(DE-STW)12921-3">
      <topic>Möbelindustrie</topic>
   </subject>
   <subject xmlns:xlink="http://www.w3.org/1999/xlink"
             authority="idref"
             xlink:href="(IDREF)027255468">
      <topic>Vannerie</topic>
   </subject>
   <subject xmlns:xlink="http://www.w3.org/1999/xlink"
             authority="gnd"
             xlink:href="(DE-588)4165301-4">
      <topic>Korbmacher</topic>
   </subject>
   <subject xmlns:xlink="http://www.w3.org/1999/xlink"
             authority="gnd"
             xlink:href="(DE-588)4021881-8">
      <geographic>Graubünden</geographic>
   </subject>
   <subject authority="idszbz">
      <geographic>Vorderrhein (Tal, Graubünden, Schweiz)</geographic>
   </subject>
   <classification authority="ddc" edition="15">390</classification>
   <classification authority="rvk">LB 94150</classification>
   <relatedItem type="series">
      <titleInfo>
         <title>Reihe Altes Handwerk</title>
         <partNumber>Heft 65</partNumber>
      </titleInfo>
   </relatedItem>
   <relatedItem type="series">
      <titleInfo>
         <title>Altes Handwerk</title>
         <partNumber>65</partNumber>
      </titleInfo>
   </relatedItem>
   <identifier type="isbn">3905342227</identifier>
   <recordInfo>
      <descriptionStandard>kids</descriptionStandard>
      <recordContentSource authority="marcorg">SzZuIDS BS/BE A258</recordContentSource>
      <recordCreationDate encoding="marc">201012</recordCreationDate>
      <recordIdentifier>9931231890105504</recordIdentifier>
      <recordOrigin>Converted from MARCXML to MODS version 3.8 using
					MARC21slim_MODS3-8_XSL2-0.xsl (Revision 2.77 2023/02/08)</recordOrigin>
   </recordInfo>
</mods>
{{< / highlight >}}