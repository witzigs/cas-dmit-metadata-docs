---
title: "BIBFRAME, JSON-LD"
weight: 41
draft: true
---

{{< highlight go-html-template "lineNos=inline" >}}
[
{
"@id": "_:N8ac0bd2300214a089264b6f82b7416a1",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Local"
],
"http://id.loc.gov/ontologies/bibframe/assigner": [
{
"@id": "http://id.loc.gov/vocabulary/organizations/idsbb"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "003123189DSV01"
}
]
},
{
"@id": "_:N469c737fabf4440e8627d0ae6aeb96d0",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:N5a388bd04c2f497dbc0584119e27dbdb"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "12921-3"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Hub490-36",
"@type": [
"http://id.loc.gov/ontologies/bflc/Uncontrolled",
"http://id.loc.gov/ontologies/bibframe/Series"
],
"http://id.loc.gov/ontologies/bibframe/status": [
{
"@id": "http://id.loc.gov/vocabulary/mstatus/t"
}
],
"http://id.loc.gov/ontologies/bibframe/title": [
{
"@id": "_:N5bd82dd58e074183a949ea03b4a14d2e"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/genreFormSchemes/rdacontent",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
]
},
{
"@id": "_:Nccd1fe3b808144c89f3ca182e3c4bb29",
"@type": [
"http://id.loc.gov/ontologies/bibframe/AdminMetadata"
],
"http://id.loc.gov/ontologies/bflc/encodingLevel": [
{
"@id": "http://id.loc.gov/vocabulary/menclvl/f"
}
],
"http://id.loc.gov/ontologies/bibframe/assigner": [
{
"@id": "_:Nbb3fd46848ae4b09b23ce5cc9bcc8c90"
}
],
"http://id.loc.gov/ontologies/bibframe/changeDate": [
{
"@type": "http://www.w3.org/2001/XMLSchema#dateTime",
"@value": "2023-08-10T04:34:59"
}
],
"http://id.loc.gov/ontologies/bibframe/creationDate": [
{
"@type": "http://www.w3.org/2001/XMLSchema#date",
"@value": "2020-10-12"
}
],
"http://id.loc.gov/ontologies/bibframe/descriptionConventions": [
{
"@id": "http://id.loc.gov/vocabulary/descriptionConventions/kids"
},
{
"@id": "http://id.loc.gov/vocabulary/descriptionConventions/isbd"
}
],
"http://id.loc.gov/ontologies/bibframe/descriptionModifier": [
{
"@id": "_:N6320f72a4fc94258ade9a0217fa043aa"
}
],
"http://id.loc.gov/ontologies/bibframe/generationProcess": [
{
"@id": "_:N9f0578fe568e4c0b8894573c05283b7c"
}
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:N5ef4bd1e87ef49d2a272c46c5551366c"
}
],
"http://id.loc.gov/ontologies/bibframe/status": [
{
"@id": "http://id.loc.gov/vocabulary/mstatus/n"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/contentTypes/txt",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Content"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/genreFormSchemes/rdacontent"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "text"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/genreFormSchemes/rdamedia",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
]
},
{
"@id": "_:Ne620ce691bd14df4b0ec210029ebd618",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Extent"
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "50 S."
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/stw",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "stw"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/gnd",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "gnd"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/mstatus/t",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Status"
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "transcribed"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/bf/works/9931231890105504?institute=41SLSP_UBS",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Work",
"http://id.loc.gov/ontologies/bibframe/Text",
"http://id.loc.gov/ontologies/bibframe/Monograph"
],
"http://id.loc.gov/ontologies/bflc/relationship": [
{
"@id": "_:N4f6859f3f8ca46f48866a0225d43e5ad"
},
{
"@id": "_:Ncb1a1ef7875942e793d09fea564b6414"
}
],
"http://id.loc.gov/ontologies/bibframe/adminMetadata": [
{
"@id": "_:Nccd1fe3b808144c89f3ca182e3c4bb29"
}
],
"http://id.loc.gov/ontologies/bibframe/classification": [
{
"@id": "_:N0f2a77be6be4469cadb4825056f70ade"
},
{
"@id": "_:Nfab01a09245448d090e15bd668e41eeb"
}
],
"http://id.loc.gov/ontologies/bibframe/content": [
{
"@id": "http://id.loc.gov/vocabulary/contentTypes/txt"
}
],
"http://id.loc.gov/ontologies/bibframe/contribution": [
{
"@id": "_:Nea59641f00c24e478c2e7646bee9a189"
},
{
"@id": "_:N2786204593ea436ba224815171a10927"
},
{
"@id": "_:N2ea55deed4944ac8b5de0fc6d8893fed"
}
],
"http://id.loc.gov/ontologies/bibframe/genreForm": [
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#GenreForm655-67"
}
],
"http://id.loc.gov/ontologies/bibframe/hasInstance": [
{
"@id": "https://eu03.alma.exlibrisgroup.com/bf/instances/9931231890105504?institute=41SLSP_UBS"
}
],
"http://id.loc.gov/ontologies/bibframe/language": [
{
"@id": "_:N50f3282e4ba34b56a93bc1174cd89913"
},
{
"@id": "http://id.loc.gov/vocabulary/languages/ger"
},
{
"@id": "_:N7665501237414e18b03d2f3ec8b79637"
}
],
"http://id.loc.gov/ontologies/bibframe/subject": [
{
"@id": "http://d-nb.info/gnd/041653017"
},
{
"@id": "http://d-nb.info/gnd/040218813"
},
{
"@id": "https://www.idref.fr/027255468"
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Place651-64"
},
{
"@id": "http://d-nb.info/gnd/042234328"
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic600-39"
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic650-48"
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic650-47"
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Temporal648-40"
}
],
"http://id.loc.gov/ontologies/bibframe/title": [
{
"@id": "_:Ndb1fd4ef70564aa984368d1350bc9f4f"
},
{
"@id": "_:N2f0d85a98e8a4dfc87a0db97c3cee206"
}
]
},
{
"@id": "_:N34338ebbca514408a6d46140e3438223",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Local"
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "237938634"
}
]
},
{
"@id": "_:N2f0d85a98e8a4dfc87a0db97c3cee206",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Title"
],
"http://id.loc.gov/ontologies/bibframe/mainTitle": [
{
"@value": "<<Der>> Korbmacher"
}
]
},
{
"@id": "_:Ne2abaadf1209423daaf3fc8bfc8757a6",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Title"
],
"http://id.loc.gov/ontologies/bibframe/mainTitle": [
{
"@value": "<<Der>> Korbmacher"
}
],
"http://id.loc.gov/ontologies/bibframe/subtitle": [
{
"@value": "Il canistrer : (1942)"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/carriers/nc",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Carrier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/genreFormSchemes/rdacarrier"
}
]
},
{
"@id": "_:N2ea55deed4944ac8b5de0fc6d8893fed",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Contribution"
],
"http://id.loc.gov/ontologies/bibframe/agent": [
{
"@id": "http://d-nb.info/gnd/118176773"
}
],
"http://id.loc.gov/ontologies/bibframe/role": [
{
"@id": "http://id.loc.gov/vocabulary/relators/ctb"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/mstatus/n",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Status"
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "new"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/genreFormSchemes/rdacarrier",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
]
},
{
"@id": "_:Ncb1a1ef7875942e793d09fea564b6414",
"@type": [
"http://id.loc.gov/ontologies/bflc/Relationship"
],
"http://id.loc.gov/ontologies/bflc/relation": [
{
"@id": "http://id.loc.gov/ontologies/bibframe/hasSeries"
}
],
"http://id.loc.gov/ontologies/bibframe/relatedTo": [
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Hub490-36"
}
],
"http://id.loc.gov/ontologies/bibframe/seriesEnumeration": [
{
"@value": "Heft 65"
}
]
},
{
"@id": "_:N5a388bd04c2f497dbc0584119e27dbdb",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "DE-STW"
}
]
},
{
"@id": "_:N71362377746245c4b4d57243afa674b8",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "DE-588"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Place651-64",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Place",
"http://www.loc.gov/mads/rdf/v1#Geographic"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/idszbz"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Vorderrhein (Tal, Graubünden, Schweiz)"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Vorderrhein (Tal, Graubünden, Schweiz)"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic600-39",
"@type": [
"http://www.loc.gov/mads/rdf/v1#ComplexSubject",
"http://id.loc.gov/ontologies/bibframe/Topic"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/idsbb"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Maissen, Alfons"
}
],
"http://www.loc.gov/mads/rdf/v1#componentList": [
{
"@list": [
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Agent600-39"
}
]
}
]
},
{
"@id": "_:N7f68fa9063b54545b2a4065a8b869228",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "DE-STW"
}
]
},
{
"@id": "_:Ndc72dddc22d6402b908f6ddc3e5b9d23",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "IDREF"
}
]
},
{
"@id": "_:N5bd82dd58e074183a949ea03b4a14d2e",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Title"
],
"http://id.loc.gov/ontologies/bibframe/mainTitle": [
{
"@value": "Reihe Altes Handwerk"
}
]
},
{
"@id": "_:Nc1c585f1b27447e7a1b49b8a56e2b684",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "rvk"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/organizations/idsbb",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Agent"
]
},
{
"@id": "_:N24127d25903d4d1aa7d82be69940bf53",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:Ndc72dddc22d6402b908f6ddc3e5b9d23"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "027255468"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/bf/instances/9931231890105504?institute=41SLSP_UBS",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Instance"
],
"http://id.loc.gov/ontologies/bflc/publicationStatement": [
{
"@value": "Chur: Bündner Monatsblatt; 2004"
}
],
"http://id.loc.gov/ontologies/bibframe/carrier": [
{
"@id": "http://id.loc.gov/vocabulary/carriers/nc"
}
],
"http://id.loc.gov/ontologies/bibframe/dimensions": [
{
"@value": "25 cm"
}
],
"http://id.loc.gov/ontologies/bibframe/extent": [
{
"@id": "_:Ne620ce691bd14df4b0ec210029ebd618"
}
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:N34338ebbca514408a6d46140e3438223"
},
{
"@id": "_:Nac1d638c9f3e4f409096a64db109ff92"
},
{
"@id": "_:N8ac0bd2300214a089264b6f82b7416a1"
}
],
"http://id.loc.gov/ontologies/bibframe/instanceOf": [
{
"@id": "https://eu03.alma.exlibrisgroup.com/bf/works/9931231890105504?institute=41SLSP_UBS"
}
],
"http://id.loc.gov/ontologies/bibframe/issuance": [
{
"@id": "http://id.loc.gov/vocabulary/issuance/mono"
}
],
"http://id.loc.gov/ontologies/bibframe/media": [
{
"@id": "http://id.loc.gov/vocabulary/mediaTypes/n"
}
],
"http://id.loc.gov/ontologies/bibframe/note": [
{
"@id": "_:Nab21dcdfc16341b68ad9b45dcb21d351"
},
{
"@id": "_:Nac126cbc7b5e4d5daca781a0929513de"
}
],
"http://id.loc.gov/ontologies/bibframe/provisionActivity": [
{
"@id": "_:Ne6d06a717a5e4206a90bd2110aa76f65"
}
],
"http://id.loc.gov/ontologies/bibframe/responsibilityStatement": [
{
"@value": "Alfons Maissen, Anna-Pia Maissen ; [Verein für Bündner Kulturforschung und Museum\n            Regiunal Surselva]\n        "
}
],
"http://id.loc.gov/ontologies/bibframe/title": [
{
"@id": "_:Ne2abaadf1209423daaf3fc8bfc8757a6"
}
]
},
{
"@id": "_:N6d48e317208843e4bb6074a1ebd82041",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:Nfdb37faf831f4937a8c6b6ac0ee9b7fa"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "4165301-4"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/languages/ger",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Language"
]
},
{
"@id": "_:N5ef4bd1e87ef49d2a272c46c5551366c",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Local"
],
"http://id.loc.gov/ontologies/bibframe/assigner": [
{
"@id": "http://id.loc.gov/vocabulary/organizations/dlc"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "9931231890105504"
}
]
},
{
"@id": "_:N4f6859f3f8ca46f48866a0225d43e5ad",
"@type": [
"http://id.loc.gov/ontologies/bflc/Relationship"
],
"http://id.loc.gov/ontologies/bflc/relation": [
{
"@id": "http://id.loc.gov/ontologies/bibframe/hasSeries"
}
],
"http://id.loc.gov/ontologies/bibframe/relatedTo": [
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Hub830-77"
}
],
"http://id.loc.gov/ontologies/bibframe/seriesEnumeration": [
{
"@value": "65"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/idsbb",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "idsbb"
}
]
},
{
"@id": "http://d-nb.info/gnd/943415527",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Agent",
"http://id.loc.gov/ontologies/bibframe/Organization"
],
"http://id.loc.gov/ontologies/bflc/marcKey": [
{
"@value": "7102 $aVerein für Bündner\n                            Kulturforschung$0(DE-588)2143724-5$0http://d-nb.info/gnd/943415527$0http://viaf.org/viaf/sourceID/DNB|943415527\n                        "
}
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:Nf8564abbd6704661b4db0f567c9d87be"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Verein für Bündner Kulturforschung"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic650-48",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Topic",
"http://www.loc.gov/mads/rdf/v1#Topic"
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:N469c737fabf4440e8627d0ae6aeb96d0"
}
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/stw"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Möbelindustrie"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Möbelindustrie"
}
]
},
{
"@id": "_:Nfab01a09245448d090e15bd668e41eeb",
"@type": [
"http://id.loc.gov/ontologies/bibframe/ClassificationDdc"
],
"http://id.loc.gov/ontologies/bibframe/classificationPortion": [
{
"@value": "390"
}
],
"http://id.loc.gov/ontologies/bibframe/edition": [
{
"@value": "abridged"
}
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:Nc736acc8df5e418e9a4280341b5eeca5"
}
]
},
{
"@id": "http://d-nb.info/gnd/041653017",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Topic",
"http://www.loc.gov/mads/rdf/v1#Topic"
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:N6d48e317208843e4bb6074a1ebd82041"
}
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/gnd"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Korbmacher"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Korbmacher"
}
]
},
{
"@id": "_:N0f2a77be6be4469cadb4825056f70ade",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Classification"
],
"http://id.loc.gov/ontologies/bibframe/classificationPortion": [
{
"@value": "LB 94150"
}
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:Nc1c585f1b27447e7a1b49b8a56e2b684"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic650-47",
"@type": [
"http://www.loc.gov/mads/rdf/v1#Topic",
"http://id.loc.gov/ontologies/bibframe/Topic"
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:Neab8eef40bf042169f1c69dfc938d5e8"
}
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/stw"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Holzhandwerk"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Holzhandwerk"
}
]
},
{
"@id": "http://d-nb.info/gnd/10148318X",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Agent",
"http://id.loc.gov/ontologies/bibframe/Person"
],
"http://id.loc.gov/ontologies/bflc/marcKey": [
{
"@value": "1001 $aMaissen,\n                            Alfons$d1905-2003$0(DE-588)10148318X$6880-01$0http://d-nb.info/gnd/10148318X$0http://viaf.org/viaf/sourceID/DNB|10148318X\n                        "
}
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:Nf00700d6b3f74b269615666c57f863f4"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Maissen, Alfons 1905-2003"
}
]
},
{
"@id": "https://www.idref.fr/027255468",
"@type": [
"http://www.loc.gov/mads/rdf/v1#Topic",
"http://id.loc.gov/ontologies/bibframe/Topic"
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:N24127d25903d4d1aa7d82be69940bf53"
}
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/idref"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Vannerie"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Vannerie"
}
]
},
{
"@id": "_:Nfba2cb8ee8204e3b9b1ea54ed20023f9",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "DE-588"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Hub830-77",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Series",
"http://id.loc.gov/ontologies/bibframe/Hub"
],
"http://id.loc.gov/ontologies/bflc/marcKey": [
{
"@value": "830 0$aAltes Handwerk$v65$w9910079023397055011"
}
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:N4adda0f5e7934e9d9b44d61d19506242"
}
],
"http://id.loc.gov/ontologies/bibframe/title": [
{
"@id": "_:N9c829a599e1f491faa0e68e19b299dbf"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/relators/ctb",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Role"
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#GenreForm655-67",
"@type": [
"http://id.loc.gov/ontologies/bibframe/GenreForm",
"http://www.loc.gov/mads/rdf/v1#GenreForm"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/genreFormSchemes/gnd-content"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Bildband"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Bildband"
}
]
},
{
"@id": "http://d-nb.info/gnd/040218813",
"@type": [
"http://www.loc.gov/mads/rdf/v1#Geographic",
"http://id.loc.gov/ontologies/bibframe/Place"
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:N33e89b5cb84d4a03838e45c86a41d728"
}
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/gnd"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Graubünden"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Graubünden"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/descriptionConventions/kids",
"@type": [
"http://id.loc.gov/ontologies/bibframe/DescriptionConventions"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "kids"
}
]
},
{
"@id": "_:N4adda0f5e7934e9d9b44d61d19506242",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "991007902339705501"
}
]
},
{
"@id": "_:Nfdb37faf831f4937a8c6b6ac0ee9b7fa",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "DE-588"
}
]
},
{
"@id": "_:Nf8564abbd6704661b4db0f567c9d87be",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:N71362377746245c4b4d57243afa674b8"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "2143724-5"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/mediaTypes/n",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Media"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/genreFormSchemes/rdamedia"
}
]
},
{
"@id": "http://d-nb.info/gnd/118176773",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Person",
"http://id.loc.gov/ontologies/bibframe/Agent"
],
"http://id.loc.gov/ontologies/bflc/marcKey": [
{
"@value": "7001 $aMaissen, Anna\n                            Pia$d1957-$0(DE-588)118176773$6880-02$0http://d-nb.info/gnd/118176773$0http://viaf.org/viaf/sourceID/DNB|118176773\n                        "
}
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:N56a92154fb344c258e60d5ab0fa3009c"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Maissen, Anna Pia 1957-"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/idref",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "idref"
}
]
},
{
"@id": "_:N37fc14c6c9094dbf89992aea8e2ae6e2",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "DE-588"
}
]
},
{
"@id": "_:Nac126cbc7b5e4d5daca781a0929513de",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Note"
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Teilw. deutscher und rätoromanischer Paralleltext"
}
]
},
{
"@id": "_:N7665501237414e18b03d2f3ec8b79637",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Language"
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@id": "http://id.loc.gov/vocabulary/languages/roh"
}
]
},
{
"@id": "_:Nea59641f00c24e478c2e7646bee9a189",
"@type": [
"http://id.loc.gov/ontologies/bflc/PrimaryContribution",
"http://id.loc.gov/ontologies/bibframe/Contribution"
],
"http://id.loc.gov/ontologies/bibframe/agent": [
{
"@id": "http://d-nb.info/gnd/10148318X"
}
],
"http://id.loc.gov/ontologies/bibframe/role": [
{
"@id": "http://id.loc.gov/vocabulary/relators/ctb"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Agent600-39",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Agent",
"http://id.loc.gov/ontologies/bibframe/Person",
"http://www.loc.gov/mads/rdf/v1#PersonalName"
],
"http://id.loc.gov/ontologies/bflc/marcKey": [
{
"@value": "60017$aMaissen, Alfons$2idsbb"
}
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/idsbb"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Maissen, Alfons"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Maissen, Alfons"
}
]
},
{
"@id": "http://id.loc.gov/ontologies/bibframe/hasSeries",
"@type": [
"http://id.loc.gov/ontologies/bflc/Relation"
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Has Series"
}
]
},
{
"@id": "_:N6320f72a4fc94258ade9a0217fa043aa",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Agent"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "CH-ZuSLS"
}
]
},
{
"@id": "http://d-nb.info/gnd/042234328",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Topic",
"http://www.loc.gov/mads/rdf/v1#Topic"
],
"http://id.loc.gov/ontologies/bibframe/identifiedBy": [
{
"@id": "_:N0a5c06c4289a44dba94f58dc216dcc1b"
}
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/gnd"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Korbmacherhandwerk"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Korbmacherhandwerk"
}
]
},
{
"@id": "_:Ndb1fd4ef70564aa984368d1350bc9f4f",
"@type": [
"http://id.loc.gov/ontologies/bibframe/ParallelTitle"
],
"http://id.loc.gov/ontologies/bibframe/mainTitle": [
{
"@value": "<<Il>> canistrer"
}
]
},
{
"@id": "_:N56a92154fb344c258e60d5ab0fa3009c",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:N01f6d195fee747b998d92911585a4ce4"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "118176773"
}
]
},
{
"@id": "_:N0a5c06c4289a44dba94f58dc216dcc1b",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:N54b1b7edcb35407381e12972e2f7b9e2"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "4223432-3"
}
]
},
{
"@id": "_:Nbb3fd46848ae4b09b23ce5cc9bcc8c90",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Agent"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "SzZuIDS BS/BE A258"
}
]
},
{
"@id": "_:Nac1d638c9f3e4f409096a64db109ff92",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Isbn"
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "3905342227"
}
]
},
{
"@id": "_:Nc736acc8df5e418e9a4280341b5eeca5",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "15"
}
]
},
{
"@id": "https://eu03.alma.exlibrisgroup.com/9931231890105504#Temporal648-40",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Temporal",
"http://www.loc.gov/mads/rdf/v1#Temporal"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/idszbz"
}
],
"http://www.loc.gov/mads/rdf/v1#authoritativeLabel": [
{
"@value": "Geschichte 1942"
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Geschichte 1942"
}
]
},
{
"@id": "_:Nf00700d6b3f74b269615666c57f863f4",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:Nfba2cb8ee8204e3b9b1ea54ed20023f9"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "10148318X"
}
]
},
{
"@id": "_:N9c829a599e1f491faa0e68e19b299dbf",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Title"
],
"http://id.loc.gov/ontologies/bibframe/mainTitle": [
{
"@value": "Altes Handwerk"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/subjectSchemes/idszbz",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "idszbz"
}
]
},
{
"@id": "_:Nab21dcdfc16341b68ad9b45dcb21d351",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Note",
"http://id.loc.gov/vocabulary/mnotetype/physical"
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "Ill."
}
]
},
{
"@id": "_:Neab8eef40bf042169f1c69dfc938d5e8",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:N7f68fa9063b54545b2a4065a8b869228"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "13109-6"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/organizations/dlc",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Agent"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "DLC"
}
]
},
{
"@id": "_:Ne6d06a717a5e4206a90bd2110aa76f65",
"@type": [
"http://id.loc.gov/ontologies/bibframe/ProvisionActivity",
"http://id.loc.gov/ontologies/bibframe/Publication"
],
"http://id.loc.gov/ontologies/bflc/simpleAgent": [
{
"@value": "Bündner Monatsblatt"
}
],
"http://id.loc.gov/ontologies/bflc/simpleDate": [
{
"@value": "2004"
}
],
"http://id.loc.gov/ontologies/bflc/simplePlace": [
{
"@value": "Chur"
}
],
"http://id.loc.gov/ontologies/bibframe/date": [
{
"@type": "http://id.loc.gov/datatypes/edtf",
"@value": "2004"
}
],
"http://id.loc.gov/ontologies/bibframe/place": [
{
"@id": "http://id.loc.gov/vocabulary/countries/sz"
}
]
},
{
"@id": "_:N9f0578fe568e4c0b8894573c05283b7c",
"@type": [
"http://id.loc.gov/ontologies/bibframe/GenerationProcess"
],
"http://id.loc.gov/ontologies/bibframe/generationDate": [
{
"@type": "http://www.w3.org/2001/XMLSchema#dateTime",
"@value": "\n                            2024-02-19T12:43:39.620114Z\n                        "
}
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "DLC marc2bibframe2 v2.4.0"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/descriptionConventions/isbd",
"@type": [
"http://id.loc.gov/ontologies/bibframe/DescriptionConventions"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "isbd"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/genreFormSchemes/gnd-content",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "gnd-content"
}
]
},
{
"@id": "_:N01f6d195fee747b998d92911585a4ce4",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "DE-588"
}
]
},
{
"@id": "_:N54b1b7edcb35407381e12972e2f7b9e2",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Source"
],
"http://id.loc.gov/ontologies/bibframe/code": [
{
"@value": "DE-588"
}
]
},
{
"@id": "http://id.loc.gov/vocabulary/menclvl/f",
"@type": [
"http://id.loc.gov/ontologies/bflc/EncodingLevel"
],
"http://www.w3.org/2000/01/rdf-schema#label": [
{
"@value": "full"
}
]
},
{
"@id": "_:N33e89b5cb84d4a03838e45c86a41d728",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Identifier"
],
"http://id.loc.gov/ontologies/bibframe/source": [
{
"@id": "_:N37fc14c6c9094dbf89992aea8e2ae6e2"
}
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@value": "4021881-8"
}
]
},
{
"@id": "_:N50f3282e4ba34b56a93bc1174cd89913",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Language"
],
"http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
{
"@id": "http://id.loc.gov/vocabulary/languages/ger"
}
]
},
{
"@id": "_:N2786204593ea436ba224815171a10927",
"@type": [
"http://id.loc.gov/ontologies/bibframe/Contribution"
],
"http://id.loc.gov/ontologies/bibframe/agent": [
{
"@id": "http://d-nb.info/gnd/943415527"
}
],
"http://id.loc.gov/ontologies/bibframe/role": [
{
"@id": "http://id.loc.gov/vocabulary/relators/ctb"
}
]
}
]
{{</ highlight >}}
