---
title: "Dublin Core, XML"
weight: 20
---
Das Mapping wurde anhand des [Crosswalk MARC to Dublin Core (Unqualified)](https://www.loc.gov/marc/marc2dc.html) der Library of Congress vorgenommen.

{{< highlight go-html-template "lineNos=inline" >}}
<oai_dc:dc xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" xmlns:dc="http://purl.org/dc/elements/1.1/"
           xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <dc:title>Der Korbmacher = Il canistrer : (1942)</dc:title>
    <dc:contributor>Maissen, Alfons</dc:contributor>
    <dc:contributor>Maissen, Anna-Pia</dc:contributor>
    <dc:contributor>Verein für Bündner Kulturforschung</dc:contributor>
    <dc:coverage>Graubünden</dc:coverage>
    <dc:coverage>Vorderrhein (Tal, Graubünden, Schweiz)</dc:coverage>
    <dc:date>2004</dc:date>
    <dc:description>Teilw. deutscher und rätoromanischer Paralleltext</dc:description>
    <dc:identifier>3905342227</dc:identifier>
    <dc:language>ger</dc:language>
    <dc:language>roh</dc:language>
    <dc:publisher>Bündner Monatsblatt</dc:publisher>
    <dc:type>text</dc:type>
    <dc:type>Bildband</dc:type>
    <dc:subject>390</dc:subject>
    <dc:subject>LB 94150</dc:subject>
    <dc:subject>Maissen, Alfons</dc:subject>
    <dc:subject>Geschichte 1942</dc:subject>
    <dc:subject>Holzhandwerk</dc:subject>
    <dc:subject>Möbelindustrie</dc:subject>
    <dc:subject>Vannerie</dc:subject>
    <dc:subject>Korbmacher</dc:subject>
</oai_dc:dc>
{{< / highlight >}}
