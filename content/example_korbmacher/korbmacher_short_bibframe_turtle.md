---
title: "BIBFRAME, Turtle"
weight: 42
draft: true
---

{{< highlight go-html-template "lineNos=inline" >}}


@prefix mads: <http://www.loc.gov/mads/rdf/v1#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix bflc: <http://id.loc.gov/ontologies/bflc/> .

<http://d-nb.info/gnd/040218813> a bf:Place, mads:Geographic ;
rdfs:label "Graubünden" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
bf:source [
rdf:type bf:Source ;
bf:code "DE-588" ;
] ;
rdf:value "4021881-8" ;
] ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/gnd> ;
mads:authoritativeLabel "Graubünden" .

<http://d-nb.info/gnd/041653017> a bf:Topic, mads:Topic ;
rdfs:label "Korbmacher" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
bf:source [
rdf:type bf:Source ;
bf:code "DE-588" ;
] ;
rdf:value "4165301-4" ;
] ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/gnd> ;
mads:authoritativeLabel "Korbmacher" .

<http://d-nb.info/gnd/042234328> a bf:Topic, mads:Topic ;
rdfs:label "Korbmacherhandwerk" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
bf:source [
rdf:type bf:Source ;
bf:code "DE-588" ;
] ;
rdf:value "4223432-3" ;
] ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/gnd> ;
mads:authoritativeLabel "Korbmacherhandwerk" .

<http://d-nb.info/gnd/10148318X> a bf:Agent, bf:Person ;
rdfs:label "Maissen, Alfons 1905-2003" ;
bflc:marcKey """1001 $aMaissen,
Alfons$d1905-2003$0(DE-588)10148318X$6880-01$0http://d-nb.info/gnd/10148318X$0http://viaf.org/viaf/sourceID/DNB|10148318X
""" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
bf:source [
rdf:type bf:Source ;
bf:code "DE-588" ;
] ;
rdf:value "10148318X" ;
] .

<http://d-nb.info/gnd/118176773> a bf:Agent, bf:Person ;
rdfs:label "Maissen, Anna Pia 1957-" ;
bflc:marcKey """7001 $aMaissen, Anna
Pia$d1957-$0(DE-588)118176773$6880-02$0http://d-nb.info/gnd/118176773$0http://viaf.org/viaf/sourceID/DNB|118176773
""" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
bf:source [
rdf:type bf:Source ;
bf:code "DE-588" ;
] ;
rdf:value "118176773" ;
] .

<http://d-nb.info/gnd/943415527> a bf:Agent, bf:Organization ;
rdfs:label "Verein für Bündner Kulturforschung" ;
bflc:marcKey """7102 $aVerein für Bündner
Kulturforschung$0(DE-588)2143724-5$0http://d-nb.info/gnd/943415527$0http://viaf.org/viaf/sourceID/DNB|943415527
""" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
bf:source [
rdf:type bf:Source ;
bf:code "DE-588" ;
] ;
rdf:value "2143724-5" ;
] .

<http://id.loc.gov/vocabulary/carriers/nc> a bf:Carrier ;
bf:source <http://id.loc.gov/vocabulary/genreFormSchemes/rdacarrier> .

<http://id.loc.gov/vocabulary/contentTypes/txt> a bf:Content ;
rdfs:label "text" ;
bf:source <http://id.loc.gov/vocabulary/genreFormSchemes/rdacontent> .

<http://id.loc.gov/vocabulary/descriptionConventions/isbd> a bf:DescriptionConventions ;
bf:code "isbd" .

<http://id.loc.gov/vocabulary/descriptionConventions/kids> a bf:DescriptionConventions ;
bf:code "kids" .

<http://id.loc.gov/vocabulary/genreFormSchemes/gnd-content> a bf:Source ;
bf:code "gnd-content" .

<http://id.loc.gov/vocabulary/genreFormSchemes/rdacarrier> a bf:Source .

<http://id.loc.gov/vocabulary/genreFormSchemes/rdacontent> a bf:Source .

<http://id.loc.gov/vocabulary/genreFormSchemes/rdamedia> a bf:Source .

<http://id.loc.gov/vocabulary/mediaTypes/n> a bf:Media ;
bf:source <http://id.loc.gov/vocabulary/genreFormSchemes/rdamedia> .

<http://id.loc.gov/vocabulary/menclvl/f> a bflc:EncodingLevel ;
rdfs:label "full" .

<http://id.loc.gov/vocabulary/mstatus/n> a bf:Status ;
rdfs:label "new" .

<http://id.loc.gov/vocabulary/mstatus/t> a bf:Status ;
rdfs:label "transcribed" .

<http://id.loc.gov/vocabulary/organizations/dlc> a bf:Agent ;
bf:code "DLC" .

<http://id.loc.gov/vocabulary/organizations/idsbb> a bf:Agent .

<http://id.loc.gov/vocabulary/subjectSchemes/idref> a bf:Source ;
bf:code "idref" .

<https://eu03.alma.exlibrisgroup.com/9931231890105504#Agent600-39> a bf:Agent, bf:Person, mads:PersonalName ;
rdfs:label "Maissen, Alfons" ;
bflc:marcKey "60017$aMaissen, Alfons$2idsbb" ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/idsbb> ;
mads:authoritativeLabel "Maissen, Alfons" .

<https://eu03.alma.exlibrisgroup.com/9931231890105504#GenreForm655-67> a bf:GenreForm, mads:GenreForm ;
rdfs:label "Bildband" ;
bf:source <http://id.loc.gov/vocabulary/genreFormSchemes/gnd-content> ;
mads:authoritativeLabel "Bildband" .

<https://eu03.alma.exlibrisgroup.com/9931231890105504#Hub490-36> a bflc:Uncontrolled, bf:Series ;
bf:status <http://id.loc.gov/vocabulary/mstatus/t> ;
bf:title [
rdf:type bf:Title ;
bf:mainTitle "Reihe Altes Handwerk" ;
] .

<https://eu03.alma.exlibrisgroup.com/9931231890105504#Hub830-77> a bf:Hub, bf:Series ;
bflc:marcKey "830 0$aAltes Handwerk$v65$w9910079023397055011" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
rdf:value "991007902339705501" ;
] ;
bf:title [
rdf:type bf:Title ;
bf:mainTitle "Altes Handwerk" ;
] .

<https://eu03.alma.exlibrisgroup.com/9931231890105504#Place651-64> a bf:Place, mads:Geographic ;
rdfs:label "Vorderrhein (Tal, Graubünden, Schweiz)" ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/idszbz> ;
mads:authoritativeLabel "Vorderrhein (Tal, Graubünden, Schweiz)" .

<https://eu03.alma.exlibrisgroup.com/9931231890105504#Temporal648-40> a bf:Temporal, mads:Temporal ;
rdfs:label "Geschichte 1942" ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/idszbz> ;
mads:authoritativeLabel "Geschichte 1942" .

<https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic600-39> a bf:Topic, mads:ComplexSubject ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/idsbb> ;
mads:authoritativeLabel "Maissen, Alfons" ;
mads:componentList (
<https://eu03.alma.exlibrisgroup.com/9931231890105504#Agent600-39>
) .

<https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic650-47> a bf:Topic, mads:Topic ;
rdfs:label "Holzhandwerk" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
bf:source [
rdf:type bf:Source ;
bf:code "DE-STW" ;
] ;
rdf:value "13109-6" ;
] ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/stw> ;
mads:authoritativeLabel "Holzhandwerk" .

<https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic650-48> a bf:Topic, mads:Topic ;
rdfs:label "Möbelindustrie" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
bf:source [
rdf:type bf:Source ;
bf:code "DE-STW" ;
] ;
rdf:value "12921-3" ;
] ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/stw> ;
mads:authoritativeLabel "Möbelindustrie" .

<https://eu03.alma.exlibrisgroup.com/bf/instances/9931231890105504?institute=41SLSP_UBS> a bf:Instance ;
bflc:publicationStatement "Chur: Bündner Monatsblatt; 2004" ;
bf:carrier <http://id.loc.gov/vocabulary/carriers/nc> ;
bf:dimensions "25 cm" ;
bf:extent [
rdf:type bf:Extent ;
rdfs:label "50 S." ;
] ;
bf:identifiedBy [
rdf:type bf:Local ;
rdf:value "237938634" ;
], [
rdf:type bf:Local ;
bf:assigner <http://id.loc.gov/vocabulary/organizations/idsbb> ;
rdf:value "003123189DSV01" ;
], [
rdf:type bf:Isbn ;
rdf:value "3905342227" ;
] ;
bf:instanceOf <https://eu03.alma.exlibrisgroup.com/bf/works/9931231890105504?institute=41SLSP_UBS> ;
bf:issuance <http://id.loc.gov/vocabulary/issuance/mono> ;
bf:media <http://id.loc.gov/vocabulary/mediaTypes/n> ;
bf:note [
rdf:type bf:Note ;
rdfs:label "Teilw. deutscher und rätoromanischer Paralleltext" ;
], [
rdf:type bf:Note, <http://id.loc.gov/vocabulary/mnotetype/physical> ;
rdfs:label "Ill." ;
] ;
bf:provisionActivity [
rdf:type bf:ProvisionActivity, bf:Publication ;
bflc:simpleAgent "Bündner Monatsblatt" ;
bflc:simpleDate "2004" ;
bflc:simplePlace "Chur" ;
bf:date "2004"^^<http://id.loc.gov/datatypes/edtf> ;
bf:place <http://id.loc.gov/vocabulary/countries/sz> ;
] ;
bf:responsibilityStatement """Alfons Maissen, Anna-Pia Maissen ; [Verein für Bündner Kulturforschung und Museum
Regiunal Surselva]
""" ;
bf:title [
rdf:type bf:Title ;
bf:mainTitle "<<Der>> Korbmacher" ;
bf:subtitle "Il canistrer : (1942)" ;
] .

<https://eu03.alma.exlibrisgroup.com/bf/works/9931231890105504?institute=41SLSP_UBS> a bf:Monograph, bf:Text, bf:Work ;
bflc:relationship [
rdf:type bflc:Relationship ;
bflc:relation bf:hasSeries ;
bf:relatedTo <https://eu03.alma.exlibrisgroup.com/9931231890105504#Hub830-77> ;
bf:seriesEnumeration "65" ;
], [
rdf:type bflc:Relationship ;
bflc:relation bf:hasSeries ;
bf:relatedTo <https://eu03.alma.exlibrisgroup.com/9931231890105504#Hub490-36> ;
bf:seriesEnumeration "Heft 65" ;
] ;
bf:adminMetadata [
rdf:type bf:AdminMetadata ;
bflc:encodingLevel <http://id.loc.gov/vocabulary/menclvl/f> ;
bf:assigner [
rdf:type bf:Agent ;
bf:code "SzZuIDS BS/BE A258" ;
] ;
bf:changeDate "2023-08-10T04:34:59"^^xsd:dateTime ;
bf:creationDate "2020-10-12"^^xsd:date ;
bf:descriptionConventions <http://id.loc.gov/vocabulary/descriptionConventions/isbd>, <http://id.loc.gov/vocabulary/descriptionConventions/kids> ;
bf:descriptionModifier [
rdf:type bf:Agent ;
bf:code "CH-ZuSLS" ;
] ;
bf:generationProcess [
rdf:type bf:GenerationProcess ;
rdfs:label "DLC marc2bibframe2 v2.4.0" ;
bf:generationDate """
2024-02-19T12:43:39.620114Z
"""^^xsd:dateTime ;
] ;
bf:identifiedBy [
rdf:type bf:Local ;
bf:assigner <http://id.loc.gov/vocabulary/organizations/dlc> ;
rdf:value "9931231890105504" ;
] ;
bf:status <http://id.loc.gov/vocabulary/mstatus/n> ;
] ;
bf:classification [
rdf:type bf:Classification ;
bf:classificationPortion "LB 94150" ;
bf:source [
rdf:type bf:Source ;
bf:code "rvk" ;
] ;
], [
rdf:type bf:ClassificationDdc ;
bf:classificationPortion "390" ;
bf:edition "abridged" ;
bf:source [
rdf:type bf:Source ;
bf:code "15" ;
] ;
] ;
bf:content <http://id.loc.gov/vocabulary/contentTypes/txt> ;
bf:contribution [
rdf:type bf:Contribution ;
bf:agent <http://d-nb.info/gnd/118176773> ;
bf:role <http://id.loc.gov/vocabulary/relators/ctb> ;
], [
rdf:type bf:Contribution ;
bf:agent <http://d-nb.info/gnd/943415527> ;
bf:role <http://id.loc.gov/vocabulary/relators/ctb> ;
], [
rdf:type bflc:PrimaryContribution, bf:Contribution ;
bf:agent <http://d-nb.info/gnd/10148318X> ;
bf:role <http://id.loc.gov/vocabulary/relators/ctb> ;
] ;
bf:genreForm <https://eu03.alma.exlibrisgroup.com/9931231890105504#GenreForm655-67> ;
bf:hasInstance <https://eu03.alma.exlibrisgroup.com/bf/instances/9931231890105504?institute=41SLSP_UBS> ;
bf:language [
rdf:type bf:Language ;
rdf:value <http://id.loc.gov/vocabulary/languages/roh> ;
], [
rdf:type bf:Language ;
rdf:value <http://id.loc.gov/vocabulary/languages/ger> ;
], <http://id.loc.gov/vocabulary/languages/ger> ;
bf:subject <http://d-nb.info/gnd/040218813>, <http://d-nb.info/gnd/041653017>, <http://d-nb.info/gnd/042234328>, <https://eu03.alma.exlibrisgroup.com/9931231890105504#Place651-64>, <https://eu03.alma.exlibrisgroup.com/9931231890105504#Temporal648-40>, <https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic600-39>, <https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic650-47>, <https://eu03.alma.exlibrisgroup.com/9931231890105504#Topic650-48>, <https://www.idref.fr/027255468> ;
bf:title [
rdf:type bf:ParallelTitle ;
bf:mainTitle "<<Il>> canistrer" ;
], [
rdf:type bf:Title ;
bf:mainTitle "<<Der>> Korbmacher" ;
] .

<https://www.idref.fr/027255468> a bf:Topic, mads:Topic ;
rdfs:label "Vannerie" ;
bf:identifiedBy [
rdf:type bf:Identifier ;
bf:source [
rdf:type bf:Source ;
bf:code "IDREF" ;
] ;
rdf:value "027255468" ;
] ;
bf:source <http://id.loc.gov/vocabulary/subjectSchemes/idref> ;
mads:authoritativeLabel "Vannerie" .

bf:hasSeries a bflc:Relation ;
rdfs:label "Has Series" .

<http://id.loc.gov/vocabulary/languages/ger> a bf:Language .

<http://id.loc.gov/vocabulary/subjectSchemes/idsbb> a bf:Source ;
bf:code "idsbb" .

<http://id.loc.gov/vocabulary/subjectSchemes/idszbz> a bf:Source ;
bf:code "idszbz" .

<http://id.loc.gov/vocabulary/subjectSchemes/stw> a bf:Source ;
bf:code "stw" .

<http://id.loc.gov/vocabulary/relators/ctb> a bf:Role .

<http://id.loc.gov/vocabulary/subjectSchemes/gnd> a bf:Source ;
bf:code "gnd" .


{{</ highlight >}}