---
title: "Systemspezifische Schnittstellen"
date: 2022-02-28T17:28:11+02:00
draft: true
---

* Schnittstellen basierend auf Prinzipien von REST
* WebHooks
* …
* weniger standardisiert
* können auch schreibenden Zugriff ermöglichen (POST, DELETE) -> Benutzerfunktionalitäten, Alma APIs für Veränderung Daten
