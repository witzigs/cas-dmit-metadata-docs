---
title: "Parameter und Query"
---

[Vollständige Dokumentation SRU 1.2](https://www.loc.gov/standards/sru/sru-1-2.html)

## Aufbau URL


|https://<span>slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK|?version=1.2|&operation=searchRetrieve|&recordSchema=marcxml|&query=alma.isbn=9783836543231
|---|---|---|---|---|
|Base URL|Parameter 1|Parameter 2|Parameter 3|Parameter 4|
   
Beispiele:

* https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.title=architektur&startRecord=11
* https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=unimarcxml&query=alma.subjects==architektur

## Informationen zur Schnittstelle (explain)
* operation: Immer explain
* version: Die SRU-Version
* Beispiel: https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=explain

## Suche (searchRetrieve)
### Verpflichtende Parameter
* operation: Immer searchRetrieve
* version: Die SRU-Version
* query: Die Suchanfrage in CQL, d.h. Index und Suchbegriff und Operator

### Optionale Parameter
* recordPacking: normalerweise XML
    * Beispiel: https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=unimarcxml&query=alma.subjects==architektur&recordPacking=xml
* recordSchema: das gewünschte Metadatenformat
    * Beispiel: https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=unimarcxml&query=alma.subjects==architektur
* startRecord: ab welcher Aufnahme aus einer Trefferliste Resultate ausgegeben werden sollen
    * Beispiel: https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.title=architektur&startRecord=11
* maximumRecords: Anzahl Aufnahmen, die ausgegeben werden sollen
    * Beispiel: http://sru.k10plus.de/opac-de-627?version=1.2?version=1.2&operation=searchRetrieve&recordSchema=marcxml&maximumRecords=10&query=pica.tpr=%22Eine%20neue%20Orgel%22%20and%20pica.per=Brandler


## CQL

[Vollständige Dokumentation](https://www.loc.gov/standards/sru/cql/)


|alma.title|all|architektur| sortBy | alma.title| /sort.descending |
|---|---|---|---|---|------------------|
|Index|Suchoperator|Suchbegriff|Sortierung|Index| Reihenfolge      

* Suche mit einem Suchbegriff: [query=alma.title all architektur](https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.title%20all%20architektur)
* Suche mit einem Suchbegriff und Sortierung: [query=alma.title all architektur sortBy alma.title/sort.descending](https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.title%20all%20architektur%20sortBy%20alma.title/sort.descending)
* Suche mit zwei Suchbegriffen in unterschiedlichen Indices und Sortierung: [query=alma.title all Architektur and alma.creator all meier sortBy alma.creator/sort.ascending](https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.title%20all%20architektur%20and%20alma.creator%20all%20meier%20sortBy%20alma.creator/sort.ascending)

### Suchindizes

Suchindizes werden von jeder SRU-Schnittstelle konfiguriert. Die Dokumentation dazu kann in der Regel mit der Operation "explain" aufgerufen werden. [Beispiel für SLSP SRU](https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=explain)

### Suchoperatoren

[Dokumentation, Abschnitt Relations](https://www.loc.gov/standards/sru/cql/contextSets/theCqlContextSet.html)

Zeichen oder Text, zum Teil beides möglich (z.B. == entspricht exact)