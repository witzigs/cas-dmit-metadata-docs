---
title: "Beispiel"
date: 2022-02-28T17:28:11+02:00
---

silvia@ub-swi:~$ yaz-client
Z> open
Could not resolve address
Z> open tcp:aleph.unibas.ch:9909
Connecting...OK.
Sent initrequest.
Connection accepted by v3 target.
ID : 81
Name : Aleph Server/GFS/YAZ
Version: ALEPH 22/4.2.66 7fe4b7de2eb91fc87a5c53ff081b15df0c449a0a
Options: search present delSet triggerResourceCtrl scan sort extendedServices namedResultSets
Elapsed: 0.057327
Z> base IDS_UTF
Z> find @attr 1=12 004518628
Sent searchRequest.
Received SearchResponse.
Search was a success.
Number of hits: 1, setno 1
records returned: 0
Elapsed: 0.040950
Z> show
Sent presentRequest (1+1).
Records: 1
[IDS_UTF]Record type: USmarc
04603nam a22006734i 4500
003 SzZuIDS IBB
005 20200827214500.0
008 080609s1480 sz 00 ger
019 $a MedeaB400: 00288914 $5 IZ UBE/rekat-ubbe
019 $a Historischer Bestand. Exemplarspezifische Aufnahme, gesperrt für Veränderungen und das Anhängen von weiteren Exemplaren $5 08.12.2016/B404/faf
019 $a AAK: 288914 (Titel: Fridolin, Der Heilige), 46203 (Autor; mit Titel: Vita S. Fridolini deutsch), 46205 (Autor; mit Titel: Sankt Fridolins Leben) $5 IZ UBE/beubzb/bmr/03.12.2008
024 7 $a 10.3931/e-rara-22946 $2 doi
035 $a 004518628
040 $a SzZuIDS BS/BE B404 $b ger $e rda
041 1 $a ger $h lat
100 0 $a Baltherus, $c Seckinganus, $d -987 $0 (DE-588)118652206, $e Verfasser $4 aut
240 10 $a Vita Sancti Fridolini
245 10 $a Hie endet sich Sant Fridolinus leben mit der vorrede vnd gezugnuß vnd [der] grossen wunderwerck die er gewurket hat vnd die got durch in gewurket hat nach synem tode
246 1 $i Incipit: $a Im namen vatters vnd suns vnd des heyligen geistes. Dis ist die vorrede in sant Fridlins leben vnd zuknuß der warheit aller [der] nachgeschriben dingen die hie stant
246 1 $i Titel gemäss GW: $a Vita S. Fridolini, deutsch
264 1 $a [Basel] : $b [Bernhard Richel], $c [um 1480]
300 $a 82 ungezählte Seiten, 1 ungezähltes Blatt : $b 60 Illustrationen ; $c 28 cm (2°)
336 $a Text $b txt $2 rdacontent
337 $a Computermedien $b c $2 rdamedia
337 $a ohne Hilfsmittel zu benutzen $b n $2 rdamedia
338 $a Online-Ressource $b cr $2 rdacarrier
338 $a Band $b nc $2 rdacarrier
500 $a Erster Druck der Lebensbeschreibung des Fridolin von Säckingen, verfasst von Balther von Säckingen, basierend auf der von Johannes Gerster 1432 angefertigten Abschrift einer deutschen Übersetzung
500 $a Keine Titelseite, Titel gemäss Explicit
500 $a Impressum gemäss GW und ISTC
500 $a Bei der Illustration wurden einige Druckstöcke mehrfach verwendet
500 $a Signaturformel: a-d⁸, e¹⁰
500 $a Blatt e₁₀ ist unbedruckt
510 4 $a GW, $c 03226 $u http://www.gesamtkatalogderwiegendrucke.de/docs/GW03226.htm
510 4 $a ISTC, $c ib00045200 $u http://data.cerl.org/istc/ib00045200
561 $a Supralibros auf Vorderdeckel: Wappen der Familie von Lerber (Wegmann 4359 b); aus dem Nachlass von Rudolf Emanuel von Lerber (1794)
563 $a Einbandbeschreibung: Ganzbezogener Ledereinband auf Kartondeckeln (braun, Schaf, 18. Jh.), Linien- und Ornamentrollen-Blindprägung auf beiden Deckeln
563 $a Vorsätze: Vorne mit 2 fliegenden Blättern, hinten mit 3 fliegenden Blättern
563 $a Masse Einband: 27.6 x 20.5 x 2 cm
563 $a Zustand Einband: Rücken vollständig ergänzt mit Linien-Blindprägung auf neuem Leder
590 $a Buchblockbeschreibung: 41 von 42 Blättern, Foliierung in Graphit, Bogenmitten jeweils innen mit Büttenpapier entsprechend dem Vorsatzpapier verstärkt, Zählung der Illustrationen in Graphit, erste Illustration teilweise rot koloriert, rot ausgezeichnete Initiale auf Blatt a₂, weitere vorgesehene Stellen für Initialen frei gelassen, Rubrizierung nur auf Blatt a₂
590 $a Zustand Buchblock: Schimmelverfleckungen und Schädlingsfrass an den oberen Blattkanten (im hinteren Teil beschnitten und mit Büttenpapier angesetzt), Brüche an den Kanten der Innenverstärkung der Doppelblattmitten
590 $a Unvollständiges Exemplar: Das unbedruckte Blatt e₁₀ fehlt
590 $a Zusammengebunden mit: Oratio in synodo Argentinensi / Johannes Geiler von Kaysersberg. Strassburg : Heinrich Eggestein, nicht vor 18. April 1482
590 $a Frühere Signatur: Inc 48 a
700 0 $a Baltherus, $c Seckinganus, $d -987. - $t Vita Sancti Fridolini $0 (DE-588)4340119-3
700 1 $a Gerster, Johannes, $d ca. 1. H. 15. Jh. $0 (DE-588)1089850441, $e Mitwirkender $4 ctb
700 1 $a Richel, Bernhard, $d -1482 $0 (DE-588)136009115, $e Drucker $4 prt
700 3 $a Von Lerber, $c Familie $0 (DE-588)1023759969, $e Früherer Eigentümer $4 fmo
700 1 $a Lerber, Rudolf Emanuel von, $d 1763-1794 $0 (DE-588)1121241654, $e Früherer Eigentümer $4 fmo
751 $a Basel $1 (DE-588)4004617-5
909 $a hybrid-el $2 ids? I-
909 $f medeabern $2 idsbb B-
909 $a unikat $2 ids? I-
909 $f erarabe $2 idsbb B-
909 $c be-ink2 $2 idsbb B-
909 $f lza-cc0 $2 idsbb B-
852 $b B404 $c 404U3 $h MUE Inc III 66 : 1 $p BM1288639 $4 Bern UB Münstergasse $5 Magazin
856 $u http://dx.doi.org/10.3931/e-rara-22946 $z Digitalisat in e-rara

nextResultSetPosition = 0
Elapsed: 0.084399
Z> base DSV01
Z> find @attr 1=12 004518628
Sent searchRequest.
Received SearchResponse.
Search was a success.
Number of hits: 1, setno 2
records returned: 0
Elapsed: 0.036203
Z> show
Sent presentRequest (1+1).
Records: 1
[DSV01]Record type: USmarc
04525nam 22006734c 4500
001 004518628
005 20200827214500.0
007 cr |||||||||||
008 080609s1480 sz 00 ger
019 $a MedeaB400: 00288914 $5 IZ UBE/rekat-ubbe
019 $a Historischer Bestand. Exemplarspezifische Aufnahme, gesperrt f�ur Ver�anderungen und das Anh�angen von weiteren Exemplaren $5 08.12.2016/B404/faf
019 $a AAK: 288914 (Titel: Fridolin, Der Heilige), 46203 (Autor; mit Titel: Vita S. Fridolini deutsch), 46205 (Autor; mit Titel: Sankt Fridolins Leben) $5 IZ UBE/beubzb/bmr/03.12.2008
024 7 $a 10.3931/e-rara-22946 $2 doi
040 $a SzZuIDS BS/BE B404 $b ger $e rda
041 1 $a ger $h lat
100 0 $a Baltherus $c Seckinganus $d -987 $1 (DE-588)118652206 $e Verfasser $4 aut
240 10 $a Vita Sancti Fridolini
245 10 $a Hie endet sich Sant Fridolinus leben mit der vorrede vnd gezugnu� vnd [der] grossen wunderwerck die er gewurket hat vnd die got durch in gewurket hat nach synem tode
246 1 $i Incipit $a Im namen vatters vnd suns vnd des heyligen geistes. Dis ist die vorrede in sant Fridlins leben vnd zuknu� der warheit aller [der] nachgeschriben dingen die hie stant
246 1 $i Titel gem�ass GW $a Vita S. Fridolini, deutsch
264 1 $a [Basel] $b [Bernhard Richel] $c [um 1480]
300 $a 82 ungez�ahlte Seiten, 1 ungez�ahltes Blatt $b 60 Illustrationen $c 28 cm (2�)
336 $a Text $b txt $2 rdacontent
337 $a Computermedien $b c $2 rdamedia
337 $a ohne Hilfsmittel zu benutzen $b n $2 rdamedia
338 $a Online-Ressource $b cr $2 rdacarrier
338 $a Band $b nc $2 rdacarrier
500 $a Erster Druck der Lebensbeschreibung des Fridolin von S�ackingen, verfasst von Balther von S�ackingen, basierend auf der von Johannes Gerster 1432 angefertigten Abschrift einer deutschen �Ubersetzung
500 $a Keine Titelseite, Titel gem�ass Explicit
500 $a Impressum gem�ass GW und ISTC
500 $a Bei der Illustration wurden einige Druckst�ocke mehrfach verwendet
500 $a Signaturformel: a-dp8s, ep10s
500 $a Blatt eb10 sist unbedruckt
510 4 $a GW $c 03226 $u http://www.gesamtkatalogderwiegendrucke.de/docs/GW03226.htm
510 4 $a ISTC $c ib00045200 $u http://data.cerl.org/istc/ib00045200
561 $a Supralibros auf Vorderdeckel: Wappen der Familie von Lerber (Wegmann 4359 b); aus dem Nachlass von Rudolf Emanuel von Lerber (1794)
563 $a Einbandbeschreibung: Ganzbezogener Ledereinband auf Kartondeckeln (braun, Schaf, 18. Jh.), Linien- und Ornamentrollen-Blindpr�agung auf beiden Deckeln
563 $a Vors�atze: Vorne mit 2 fliegenden Bl�attern, hinten mit 3 fliegenden Bl�attern
563 $a Masse Einband: 27.6 x 20.5 x 2 cm
563 $a Zustand Einband: R�ucken vollst�andig erg�anzt mit Linien-Blindpr�agung auf neuem Leder
590 $a Buchblockbeschreibung: 41 von 42 Bl�attern, Foliierung in Graphit, Bogenmitten jeweils innen mit B�uttenpapier entsprechend dem Vorsatzpapier verst�arkt, Z�ahlung der Illustrationen in Graphit, erste Illustration teilweise rot koloriert, rot ausgezeichnete Initiale auf Blatt ab2s, weitere vorgesehene Stellen f�ur Initialen frei gelassen, Rubrizierung nur auf Blatt ab2s
590 $a Zustand Buchblock: Schimmelverfleckungen und Sch�adlingsfrass an den oberen Blattkanten (im hinteren Teil beschnitten und mit B�uttenpapier angesetzt), Br�uche an den Kanten der Innenverst�arkung der Doppelblattmitten
590 $a Unvollst�andiges Exemplar: Das unbedruckte Blatt eb10 sfehlt
590 $a Zusammengebunden mit: Oratio in synodo Argentinensi / Johannes Geiler von Kaysersberg. Strassburg : Heinrich Eggestein, nicht vor 18. April 1482
590 $a Fr�uhere Signatur: Inc 48 a
700 00 $a Baltherus $c Seckinganus $d -987 $t Vita Sancti Fridolini $1 (DE-588)4340119-3
700 1 $a Gerster, Johannes $d ca. 1. H. 15. Jh. $1 (DE-588)1089850441 $e Mitwirkender $4 ctb
700 1 $a Richel, Bernhard $d -1482 $1 (DE-588)136009115 $e Drucker $4 prt
700 3 $a Von Lerber $c Familie $1 (DE-588)1023759969 $e Fr�uherer Eigent�umer $4 fmo
700 1 $a Lerber, Rudolf Emanuel <<von>> $d 1763-1794 $1 (DE-588)1121241654 $e Fr�uherer Eigent�umer $4 fmo
751 $a Basel $1 (DE-588)4004617-5
909 I $a hybrid-el
909 B $f medeabern
909 I $a unikat
909 B $f erarabe
909 B $c be-ink2
909 B $f lza-cc0
856 $u http://dx.doi.org/10.3931/e-rara-22946 $z Digitalisat in e-rara
852 $b B404 $c 404U3 $h MUE Inc III 66 : 1 $p BM1288639 $4 Bern UB M�unstergasse $5 Magazin

nextResultSetPosition = 0
Elapsed: 0.033132
Z> exit
See you later, alligator.
