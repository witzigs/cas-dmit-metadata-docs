---
title: "Requests und Arguments"
---

[Vollständige Dokumentation](https://www.openarchives.org/OAI/openarchivesprotocol.html#ProtocolMessages)

## Aufbau URL

|https://www<span>.zora.uzh.ch/cgi/oai2|?verb=ListRecords|&metadataPrefix=oai_dc|&from=2020-07-06|
|---|---|---|---|
|Base URL des Repositories|Request|Argument 1|Argument 2|
   
Mögliche Arguments sind abhängig vom Request. Mehrere Arguments werden mit & verknüpft.

Beispiele:

* https://www.zora.uzh.ch/cgi/oai2?verb=ListRecords&metadataPrefix=oai_dc&from=2023-07-06
* https://www.zora.uzh.ch/cgi/oai2?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:www.zora.uzh.ch:2951

## Requests
* Identify: ruft Informationen über das Repository auf
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=Identify
* ListMetadataFormats: ruft eine Liste der verfügbaren Metadatenformate auf
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=ListMetadataFormats
* ListSets: ruft eine Liste der verfügbaren Sets auf
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=ListSets
* ListIdentifiers: ruft eine Liste aller Identifier auf
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=ListIdentifiers&metadataPrefix=oai_dc
* ListRecords: ruft eine Liste aller Records auf 
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=ListRecords&metadataPrefix=oai_dc
* GetRecord: ruft einen einzelnen Record auf
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=GetRecord&metadataPrefix=oai_dc&identifier=oai:www.zora.uzh.ch:2951

### Arguments für GetRecord
* identifier: für den Identifier eines Records
* metadataPrefix: für das gewünschte Metadatenformat

### Arguments für ListIdentifiers und ListRecords
* from: Datum oder Datum und Zeit (2020-05-14 oder 2020-05-14T11:41:02Z)
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=ListRecords&metadataPrefix=oai_dc&from=2023-07-06
* until: Datum oder Datum und Zeit (2020-05-14 oder 2020-05-14T11:41:02Z)
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=ListRecords&metadataPrefix=oai_dc&from=2023-07-06&until=2024-02-06
* metadataPrefix: für das gewünschte Metadatenformat
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=ListRecords&metadataPrefix=oai_dc
* set: für das gewünschte Set
    * Beispiel: https://www.zora.uzh.ch/cgi/oai2?verb=ListRecords&metadataPrefix=oai_dc&set=driver
* resumptionToken: token, das man bei der vorherigen Anfrage vom Server erhalten hat. Das Token wird am Ende der Response im Tag resumptionToken angegeben.
    * Beispiel (funktioniert nicht, da ein resumption token zeitlich begrenzt gültig ist): https://www.zora.uzh.ch/cgi/oai2?verb=ListRecords&resumptionToken=metadataPrefix%3Doai_dc%26offset%3D121%26set%3Ddriver