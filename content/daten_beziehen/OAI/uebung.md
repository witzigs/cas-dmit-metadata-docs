---
title: "Übung"
---

Beantworten Sie für eine OAI-Schnittstelle Ihrer Wahl folgende Fragen:

* Welche Metadatenformate bietet das Repository an?
* Wie sieht die Struktur der Identifier in diesem Repository aus?
* Suchen Sie auf der Suchoberfläche des Repositories nach einer Aufnahme mit dem Stichwort "metadata". Kopieren Sie den Identifier der ersten Aufnahme auf der Trefferliste und holen Sie damit diese Aufnahme über die OAI-Schnittstelle im MARC-Format ab.
* Wann wurde diese Aufnahme zuletzt bearbeitet?
* Welche Sets bietet das Repository an?
* In welchen Sets ist die vorher abgefragte Aufnahme enthalten?
* Welcher Titel wird für Records, die am letzten Montag bearbeitet wurden, zuerst aufgeführt? Was ist das Erscheinungsdatum dieser Publikation?
* Suchen Sie nach den Records, die in diesem Monat bearbeitet wurden. Holen Sie auch die nächste Seite ab.
* Speichern Sie die ausgegebenen Resultate ab (z.B. mit curl).


### Mögliche OAI-Schnittstellen
* [Helveticat OAI](https://helveticat.nb.admin.ch/view/oai/41SNL_51_INST/request) (Suchoberfläche: [Helveticat](https://helveticat.nb.admin.ch/discovery/search?vid=41SNL_51_INST:helveticat&lang=de))
* [ZORA OAI](https://www.zora.uzh.ch/cgi/oai2) (Suchoberfläche: [ZORA](https://www.zora.uzh.ch/))
* [SERVAL OAI](http://serval.unil.ch/oaiprovider) (Suchoberfläche: [SERVAL](https://serval.unil.ch/))
* [e-rara OAI](https://www.e-rara.ch/oai/) (Suchoberfläche: [e-rara](https://www.e-rara.ch/))
* [RERO doc OAI](http://doc.rero.ch/oai2d) (Suchoberfläche: [RERO doc](https://doc.rero.ch/))

### Lösungen
<br/>
<details>
  <summary>Welche Metadatenformate bietet das Repository an?</summary>
  <ul>
    <li>Mit einer Abfrage mit dem Request ListMetadataFormats kann diese Frage beantwortet werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Wie sieht die Struktur der Identifier in diesem Repository aus?</summary>
  <ul>
    <li>Meistens ist die Struktur der Identifier in den Metainformationen zum Repository, Request Identify, dokumentiert.</li>
    <li>Wenn die Struktur der Identifier nicht dokumentiert ist oder Beispiele nötig sind, hilft es, sich alle Identifiers mit dem Request ListIdentifiers anzuschauen. Daraus lässt sich die korrekte Struktur ableiten.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Suchen Sie auf der Oberfläche des Repositories nach einer Aufnahme mit dem Stichwort "metadata". Kopieren Sie den Identifier der ersten Aufnahme auf der Trefferliste und holen Sie damit diese Aufnahme über die OAI-Schnittstelle ab.</summary>
  <ul>
    <li>Dazu muss die Struktur der Identifer bekannt sein, siehe vorhergehende Frage.</li>
    <li>Anschliessend kann die Aufnahme mit dem Request GetRecord abgeholt werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Wann wurde diese Aufnahme zuletzt bearbeitet?</summary>
  <ul>
    <li>Im Header der Response wird im Tag datestamp das letzte Bearbeitungsdatum ausgegeben.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Welche Sets bietet das Repository an?</summary>
  <ul>
    <li>Sets können mit dem Request ListSets aufgelistet werden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>In welchen Sets ist die vorher abgefragte Aufnahme enthalten?</summary>
  <ul>
    <li>Die Zugehörigkeit eines Records zu einem Set ist im Header im Tag setSpec angegeben.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Welcher Titel wird für Records, die am letzten Montag bearbeitet wurden, zuerst aufgeführt? Was ist das Erscheinungsdatum dieser Publikation?</summary>
  <ul>
    <li>Mit ListRecords und den Argumenten from und until können Records, die in einem bestimmten Zeitraum bearbeitet wurden, abgeholt werden.</li>
    <li>Das Erscheinungsdatum ist in den Metadaten zu finden.</li>
  </ul>
</details>
<br/>
<details>
  <summary>Suchen Sie nach den Records, die in diesem Monat bearbeitet wurden. Holen Sie auch die nächste Seite ab.</summary>
  <ul>
    <li>Mit ListRecords und dem Argument from können Records, die seit einem bestimmten Datum bearbeitet wurden, abgeholt werden.</li>
    <li>Die nächste Seite, falls vorhanden, kann mit dem resumptionToken abgeholt werden. Das Token wird am Ende der Response im Tag resumptionToken angegeben</li>
  </ul>
</details>
<br/>
<details>
  <summary>Speichern Sie die ausgegebenen Resultate ab (z.B. mit curl).</summary>
  <ul>
    <li>Zum Beispiel: curl 'https://www.zora.uzh.ch/cgi/oai2?verb=ListRecords&metadataPrefix=oai_dc&set=driver' > results.xml</li>
  </ul>
</details>
<br/>