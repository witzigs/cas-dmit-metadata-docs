---
title: "Tools"
weight: 10
---

Eine Auswahl an möglichen Tools, die für die Analyse und Transformation von Metadaten im Bibliotheksbereich nützlich sein können. Es handelt sich nicht um eine abschliessende Liste!

## Shellbefehle
* [grep](https://wiki.ubuntuusers.de/grep/): Anzeige von Zeilen, in denen ein Regex vorkommt
* [sort](https://wiki.ubuntuusers.de/sort/): Sortierung von Zeilen
* [uniq](https://wiki.ubuntuusers.de/uniq/): Ausgabe von einzigartigen (oder alternativ doppelten) Zeilen. Achtung: Zuerst sortieren, damit uniq korrekt funktioniert.
* [cut](https://wiki.ubuntuusers.de/cut/): Zerschneiden von Zeilen basisierend auf Anzahl Zeichen oder Trennzeichen
* [sed](https://wiki.ubuntuusers.de/sed/): Suchen und Ersetzen mit Regex
* und viele weitere

## MarcEdit
* [MarcEdit](https://marcedit.reeset.net/)
* Freie Software für Windows, Linux und Mac zum Editieren von MARC-Aufnahmen und zur Umwandlung von MARC in verschiedene andere Standards und Formate (inkl. Bibframe und RDF). Bietet auch einen integrierten OAI-PMH, SRU und Z39.50 Client an. 

## Catmandu
* [Catmandu](https://librecat.org/index.html)
* Kommandozeilentool für Linux (mit Docker auch unter Windows und Mac) zum Import, Export und Manipulation von Daten in verschiedenen Formaten. Unterstützt MARC Bandformat, MARCXML, Excel, CSV, RDF sowie OAI-PMH, SRU und Z39.50 für Datenimport. Mit der integrierten Fix-Language können die Daten maschinell bearbeitet werden. Catmandu ist in Perl geschrieben und kann alternativ statt in der Kommandozeile auch in Perl-Scripte integriert werden.

## OpenRefine
* [OpenRefine](https://openrefine.org/)
* Open-Source Software für Windows, Linux und Mac zum Importieren, Bearbeiten, Bereinigen und Anreichern von Daten. Besonders geeignet für tabellarische Daten (csv, Excel). Nicht spezifisch für bibliothekare Daten entwickelt.

## KNIME
* [KNIME](https://www.knime.com/)
* Data-Science Software für Windows, Linux und Mac zur Modellierung und dem Durchführen von Workflows. Für den Eigengebrauch nach Registrierung kostenlos. Nicht spezifisch für den bibliothekarische Daten entwickelt.

## pymarc
* [pymarc](https://gitlab.com/pymarc/pymarc)
* Python Library um MARC-Daten in Python-Scripten einzulesen, zu modifzieren und auszugeben.


