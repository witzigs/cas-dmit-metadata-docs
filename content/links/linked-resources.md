---
title: "BIBFRAME Ressourcen"
weight: 20
---

Eine subjektive Auswahl von interessanten Projekten, Veranstaltungen und weiteren Ressourcen im Bereich BIBFRAME und Linked Data in Bibliotheken zur Vertiefung des Themas.

## Projekte
### LD4P Projekte
* [LD4P](https://wiki.lyrasis.org/pages/viewpage.action?pageId=74515029) Linked Data for Production
* [LD4P2](https://wiki.lyrasis.org/display/LD4P2)

### ShareVDE (Casalini)
* https://www.svde.org/
* ShareVDE Wiki: https://wiki.share-vde.org/wiki/Main_Page


## Editoren
* Library of Congress Editor: https://bibframe.org/bfe/index.html
* Sinopia: https://sinopia.io/ und Videos https://www.youtube.com/playlist?list=PLrOZtzLTYPJcBux29ezxZju9FgcifJ2pn


## Veranstaltungen
* BIBFRAME Workshop in Europe: https://www.casalini.it/bfwe2022/
* Semantic Web in Libraries: https://swib.org/


## Gremien
* BIBFRAME Interoperability Group: https://wiki.lyrasis.org/pages/viewpage.action?pageId=249135298
* IGELU LoD Working Group: https://igelu.org/products-and-initiatives/communities-of-practice/linked-open-data/

## Datenmodell
* BIBFRAME Hub vs Work: https://link.springer.com/chapter/10.1007/978-3-031-16802-4_30 und  https://doi.org/10.4403/jlis.it-12760

## Varia
* BIBFRAME history: https://www.loc.gov/item/webcast-9672/
* Mapping MARC to RDA-RDF (University of Washington Libraries): https://github.com/uwlib-cams/MARC2RDA/wiki
* Linked Data / RDF resources: https://ld4pe.dublincore.org/explore-learning-resources-by-competency/
* LRM Resources: https://www.librarianshipstudies.com/2020/04/ifla-library-reference-model-lrm.html



